package Exercici1;

public class Cercle {
	private double radius;
	private String color;
	
	public Cercle(){
		radius=2.0;
		color="blue";
	}
	
	public Cercle(double radius){
		this.radius=radius;
		color="blue";
	}
	
	public Cercle(String color){
		this.color=color;
		radius=2.0;
	}
	
	public Cercle(double radius, String color){
		this.radius=radius;
		this.color=color;
	}
	
	public double getRadius(){
		return radius;
	}
	
	public String getColor(){
		return color;
	}
	
	public double getArea(){
		double area;
		
		area = Math.pow(radius, 2) * Math.PI;
		return area;
	}
	
}
