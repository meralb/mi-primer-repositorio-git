package Exercici1;

import java.util.Scanner;

public class NumeroParellSenar {

	public static void main(String[] args) {
		int y;
		Scanner input = new Scanner(System.in);
		
		do{
			System.out.println("Introdueix un numero: ");
			y=input.nextInt();
		}while(y<1);
		
		if(y%2==0)
			System.out.println("El numero es parell.");
		else
			System.out.println("El numero es senar.");

		for(int i=1; i<=y; i++)
			System.out.println(i);
		
	}
}
