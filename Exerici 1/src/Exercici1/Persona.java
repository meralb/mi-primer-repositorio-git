package Exercici1;

public class Persona {
	private String Nom,Cognom,Dni;
	private int Edat;
	private boolean EstatCivil;
	
	public Persona(String Nom,String Cognom, String Dni, int Edat, boolean EstatCivil){
		this.Nom = Nom;
		this.Cognom = Cognom;
		this.Dni = Dni ;
		this.Edat = Edat;
		this.EstatCivil = EstatCivil;
	}
	
	public String mostraNom(){
		return this.Nom;
	}
	
	public String mostraNomComplert(){
		String NomComplert=(this.Nom+" "+this.Cognom);
		return NomComplert;
	}
	
	public String EstatCivil(){
		String estat;
		if(EstatCivil==false)
			estat="Solter";
		else
			estat="Casat";
		return estat;
	}
	
	public int Edat(int Edat){
		return Edat+1;
	}
	
	public String mostraPersona(){
		String Persona=(this.mostraNomComplert()+" "+this.EstatCivil()+" "+this.Edat(Edat)+" Anys"+" "+this.Dni);
		return Persona;
	}
}
