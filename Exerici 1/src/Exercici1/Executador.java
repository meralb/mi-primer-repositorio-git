package Exercici1;

public class Executador {

	public static void main(String[] args) {
		Cercle c1,c2,c3,c4;
		c1 = new Cercle();
		c2 = new Cercle(3.0);
		c3 = new Cercle("Green");
		c4 = new Cercle(3.0, "Green");
		
System.out.println("El area del Cercle u es: "
+ c1.getArea() + ", el radi es: " + c1.getRadius() + " i el color es: " + c1.getColor());
	
System.out.println("El area del Cercle dos es: "
+ c2.getArea() + ", el radi es: " + c2.getRadius() + " i el color es: " + c2.getColor());
	
System.out.println("El area del Cercle tres es: "
+ c3.getArea() + ", el radi es: " + c3.getRadius() + " i el color es: " + c3.getColor());	
	
System.out.println("El area del Cercle quatre es: "
+ c4.getArea() + ", el radi es: " + c4.getRadius() + " i el color es: " + c4.getColor());
	
	}

}
