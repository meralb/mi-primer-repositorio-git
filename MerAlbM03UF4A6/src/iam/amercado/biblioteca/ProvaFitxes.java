package iam.amercado.biblioteca;

public class ProvaFitxes {

	public static void main(String[] args) {

		Obra ob1 = new Obra("BB43", "What do you mean", "Charflex", (short) 342);

		System.out.println(ob1.toString());
		System.out.println("Autor: " + ob1.getAuthor());
		ob1.setAuthor("Temmo tejon");
		System.out.println("Autor: " + ob1.getAuthor());
		System.out.println("Titol: " + ob1.getTitol());

		Volum vo1 = new Volum("BB43", "Elena Nito", "Del Bosque", (short) 342, (short) 2);

		System.out.println(ob1.toString());
		System.out.println("Autor: " + vo1.getAuthor());
		vo1.setAuthor("Paquito");
		System.out.println("Autor: " + vo1.getAuthor());
		System.out.println("Titol: " + vo1.getTitol());

		Volum vo2 = new Volum("BB44", "Solfamidas", "Antonio Castelo", (short) 342, (short) 2);

		System.out.println(vo2.toString());
		System.out.println("Autor: " + vo2.getAuthor());
		vo2.setAuthor("Antoniiiiio");
		System.out.println("Autor: " + vo2.getAuthor());
		System.out.println("Titol: " + vo2.getTitol());

		Obra ob2 = new Obra("BB44", "What the fuuuuu", "Dani mateo", (short) 235);

		System.out.println(ob2.toString());
		System.out.println("Autor: " + ob2.getAuthor());
		ob2.setAuthor("D.Mateo");
		System.out.println("Autor: " + ob2.getAuthor());
		System.out.println("Titol: " + ob2.getTitol());
		
		Revista re1 = new Revista("BB44", "Solfamidas",(short) 1995, (short) 2);

		System.out.println(re1.toString());
		System.out.println("Titol: " + re1.getTitol()); 
		re1.setAny((short)2015);
		re1.getAny();


		if (ob1.equals(vo2)) {
			System.out.println("La Obra " + ob1.titol + " i el volum " + vo2.titol + "son el mateix");
		} else {
			System.out.println("La Obra " + ob1.titol + " i el volum " + vo2.titol + " no son el mateix");
		}

		if (ob2.equals(vo2)) {
			System.out.println("La Obra " + ob2.titol + " i el volum " + vo2.titol + " son el mateix");
		} else {
			System.out.println("La Obra " + ob2.titol + " i el volum " + vo2.titol + " no son el mateix");
		}

	}

}
