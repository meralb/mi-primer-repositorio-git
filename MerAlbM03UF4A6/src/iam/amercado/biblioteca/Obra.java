package iam.amercado.biblioteca;

public class Obra extends Fitxa {

	protected String author;
	protected short nrePags;
	protected int numerosobra;

	public Obra(String referencia, String titol, String author, short nrePags) {
		super(referencia, titol);
		this.author = author;
		this.nrePags = nrePags;
		numerosobra++;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public short getNrePags() {
		return nrePags;
	}

	public void setNrePags(short nrePags) {
		this.nrePags = nrePags;
	}

	public String toString() {
		return "Referencia: " + referencia;

	}

}
