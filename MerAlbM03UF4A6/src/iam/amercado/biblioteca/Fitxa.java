package iam.amercado.biblioteca;

public class Fitxa {

	protected String referencia;
	protected String titol;

	public Fitxa(String referencia, String titol) {
		this.referencia = referencia;
		this.titol = titol;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getTitol() {
		return titol;
	}

	public void setTitol(String titol) {
		this.titol = titol;
	}

	public String toString() {
		return "Fitxa [referencia= " + referencia + "]";
	}

	public boolean equals(Object obj) {

		if (obj instanceof Fitxa) {
			Fitxa fitx = (Fitxa) obj;
			if (this.referencia.equals(fitx.referencia)) {
				return true;
			}
		}

		return false;
	}

}
