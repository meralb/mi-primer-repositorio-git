package iam.amercado.biblioteca;

public class Volum extends Obra {
	private short nro;

	public Volum(String referencia, String titol, String author, short nrePags, short nro) {
		super(referencia, titol, author, nrePags);
		this.nro = nro;
	}

	public short getNro() {
		return nro;
	}

	public void setNro(short nro) {
		this.nro = nro;
	}

	public String toString() {
		return "Referencia: " + referencia;

	}
}
