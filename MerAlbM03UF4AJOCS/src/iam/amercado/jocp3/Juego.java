package iam.amercado.jocp3;

abstract class Juego {
	private int vidas;
	private int numVidasIniciales;
	private static int record = 0;

	public abstract void Juega();

	public Juego(int vidas) {
		this.vidas = vidas;
	}

	public void MuestraVidasRestantes() {
		System.out.println("Les vides son: " + vidas);
	}

	public boolean QuitaVida() {
		vidas -= 1;
		if (vidas > 0) {
			return true;
		} else
			System.out.println("Juego terminado");
		return false;
	}

	public void ReiniciaPartida() {
		numVidasIniciales = vidas;
	}

	public void ActualizaRecord() {
		if (vidas == record) {
			System.out.println("Ja tens el record!");
		} else if (vidas > record) {
			record = vidas;
			System.out.println("Acabes de superar el record, i ara es: " + record);
		} else {

		}
	}

}
