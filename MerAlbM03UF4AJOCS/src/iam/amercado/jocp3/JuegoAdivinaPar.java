package iam.amercado.jocp3;

public class JuegoAdivinaPar extends JuegoAdivinaNumero {

	public JuegoAdivinaPar(int vidas, int numAdivi) {
		super(vidas, numAdivi);
	}

	boolean ValidaNumero(int num) {
		if (num % 2 == 0) {
			return true;
		} else
			System.out.println("No pots introdueir aquest numero! ES IMPAR!");
		return false;
	}

}
