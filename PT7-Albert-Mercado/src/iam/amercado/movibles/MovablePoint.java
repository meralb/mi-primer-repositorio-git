package iam.amercado.movibles;

public class MovablePoint implements Movable {

	public int x;
	public int y;
	public int xSpeed;
	public int ySpeed;

	MovablePoint(int x, int y, int xSpeed, int ySpeed) {
		this.y = y;
		this.x = x;
		this.ySpeed = ySpeed;
		this.xSpeed = xSpeed;

	}

	public String toString() {
		String string;
		string = "X: " + x + "\nY: " + y + "\nxSpeed: " + xSpeed + "\nySpeed: " + ySpeed;
		return string;
	}

	@Override
	public void moveUP() {
		this.y += ySpeed;
	}

	@Override
	public void moveDown() {
		this.y -= ySpeed;
	}

	@Override
	public void moveLeft() {
		this.x -= xSpeed;
	}

	@Override
	public void moveRight() {
		this.x += xSpeed;
	}

}
