package iam.amercado.movibles;

public class MovableCircle implements Movable {

	private int radius;
	private MovablePoint center;

	public MovableCircle(int x, int y, int xSpeed, int ySpeed, int radius) {
		center = new MovablePoint(x, y, xSpeed, ySpeed);
		this.radius = radius;

	}

	public String toString() {

		return "X: " + center.x + "Y: " + center.y + "xSpeed: " + center.xSpeed + "ySpeed: " + center.ySpeed
				+ "Radius: " + radius;

	}

	@Override
	public void moveUP() {
		center.moveUP();
	}

	@Override
	public void moveDown() {
		center.moveDown();
	}

	@Override
	public void moveLeft() {
		center.moveLeft();
	}

	@Override
	public void moveRight() {
		center.moveRight();
	}

}
