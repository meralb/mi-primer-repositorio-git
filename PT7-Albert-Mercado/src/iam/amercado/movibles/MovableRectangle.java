package iam.amercado.movibles;

public class MovableRectangle implements Movable {
	private MovablePoint topLeft;
	private MovablePoint bottomRght;

	public MovableRectangle(int x1, int x2, int y1, int y2, int xSpeed, int ySpeed) {

		topLeft = new MovablePoint(x1, y1, xSpeed, ySpeed);
		bottomRght = new MovablePoint(x2, y2, xSpeed, ySpeed);
	}

	@Override
	public String toString() {
		String string;
		string = "X1: " + topLeft.x + "Y1: " + topLeft.y + "xSpeed: " + topLeft.xSpeed + "ySpeed: " + topLeft.ySpeed
				+ "X2: " + topLeft.x + "Y2: " + topLeft.y + "xSpeed: " + topLeft.xSpeed + "ySpeed: " + topLeft.ySpeed;
		return string;
	}

	@Override
	public void moveUP() {
		topLeft.moveUP();
		bottomRght.moveUP();
	}

	@Override
	public void moveDown() {
		topLeft.moveDown();
		bottomRght.moveDown();
	}

	@Override
	public void moveLeft() {
		topLeft.moveLeft();
		bottomRght.moveLeft();
	}

	@Override
	public void moveRight() {
		topLeft.moveRight();
		bottomRght.moveRight();
	}

}
