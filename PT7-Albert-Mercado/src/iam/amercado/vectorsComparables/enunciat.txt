Existeixen multitud de mètodes d'ordenació de vectors: heapsort, bubblesort, quicksort, directsort, ... 

Pels qui tingueu ganes d'aprendre i curiositat, podeu trobar aquests enllaços interessants:
 * Wikipedia:   http://en.wikipedia.org/wiki/Sorting_algorithm
 * Codi en C:   http://www.codebeach.com/2008/09/sorting-algorithms-in-c.html
 * Animacions:   http://www.sorting-algorithms.com/

Per exemple, la implementació en C de l'algorisme d'ordenació directe (sent “v” el vector i “n” la seva longitud) seria:

i = 0;
while (i < n-1) {
  j = i + 1;
  while (j < n) {
    if (v[j] < v[i]) {
      aux = v[i];
      v[i] = v[j];
      v[j] = aux;
    }
    j++;
  }
  i++;
}

A aquest exercici heu d'implementar:

(1) Una classe anomenada “OrdenaVector” amb métodes estàtics per ordenar vectors d'objectes i també un mètode per imprimir vectors. Aquesta classe com a mínim tindrà el mètode d'ordenació directa “directSort”. Els mètodes estàtics per treballar amb vectors rebran el vector com a paràmetre.

(2) Els elements del vector, per poder-se ordenar, han de ser del tipus “ordenable” (vull dir que implementaran la interface “Ordenable” que té el mètode “menorQue” que retorna un booleà)

(3) Els elements del vector, per poder-se imprimir, han de ser del tipus “imprimible” (vull dir que implementaran la interface “Imprimible” que té el mètode “imprimir”)

Proveu-lo ordenant algun vector d'alguna classe de les que ja teniu: “Persona”, “ObjecteGeometric”, etc.
