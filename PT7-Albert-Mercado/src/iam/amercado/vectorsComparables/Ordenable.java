package iam.amercado.vectorsComparables;

public interface Ordenable {

	public boolean menorQue(Ordenable a);
}
