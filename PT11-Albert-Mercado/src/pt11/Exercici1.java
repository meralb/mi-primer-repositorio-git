package pt11;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Exercici1 {

	public static void main(String[] args) throws IOException {

		Scanner llegir = new Scanner(System.in);
		String fitxorig, fitxcop;
		System.out.println("Introdueix quin fitxer vols llegir: ");
		fitxorig = llegir.nextLine();
		System.out.println("Introdueix a quin fitxer vols enviar el llegit: ");
		fitxcop = llegir.nextLine();

		FileReader llegit = new FileReader("/home/ausias/Escriptori/" + fitxorig);
		FileWriter escrit = new FileWriter("/home/ausias/Escriptori/" + fitxcop);

		int aux;
		while ((aux = llegit.read()) != -1) {
			if (Character.isUpperCase(aux))
				escrit.write((Character.toLowerCase(aux)));

			else
				escrit.write((Character.toUpperCase(aux)));
		}

		llegit.close();
		escrit.close();

	}
}
