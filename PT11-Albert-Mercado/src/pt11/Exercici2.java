package pt11;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

import cat.iam.ad.utilitats.Conversor;

public class Exercici2 {

	private static final String[] Alt = null;

	public static void main(String[] args) throws IOException {

		Scanner llegir = new Scanner(System.in);
		String img;
		System.out.println("Introdueix el nom d'un fitxer PNG: ");
		img = llegir.nextLine();

		FileInputStream llegit = new FileInputStream(img);

		int num[] = new int[8];

		llegit.skip(16);
		for (int i = 0; i < num.length; i++) {
			num[i] = llegit.read();
		}

		System.out.println("L'amplada és: " + Conversor.bytes2Int(num[0], num[1], num[2], num[3]));
		System.out.println("L'alçada és: " + Conversor.bytes2Int(num[4], num[5], num[6], num[7]));

		llegit.close();
	}
}
