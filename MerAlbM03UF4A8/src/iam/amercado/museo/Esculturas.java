package iam.amercado.museo;

public class Esculturas extends Obres {

	private String material;
	private double altura;

	public Esculturas(String titol, int numinv, Artista autor, int any, String material, double altura) {
		super(titol, numinv, autor,any);
		this.altura = altura;
		this.material = material;

	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}
	
	public String toString(){
		return super.getTitol()+super.getAutor()+super.getAny();
	}

}
