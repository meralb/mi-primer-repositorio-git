package iam.amercado.museo;

public class Obres {

	private String titol;
	private int numinv;
	private Artista autor;
	private int any;

	public Obres(String titol, int numinv,Artista autor,int any){
		this.titol=titol;
		this.autor=autor;
		this.numinv=numinv;
		this.any=any;
	}
	
	public String getTitol() {
		return titol;
	}

	public void setTitol(String titol) {
		this.titol = titol;
	}

	public int getNuminv() {
		return numinv;
	}

	public void setNuminv(int numinv) {
		this.numinv = numinv;
	}

	public String getAutor() {
		return autor.getNom();
	}

	public void setAutor(Artista autor) {
		this.autor = autor;
	}

	public int getAny() {
		return any;
	}

	public void setAny(int any) {
		this.any = any;
	}
	
	public String toString(){
		
		return "tostring";
	}

}
