package iam.amercado.museo;

import java.util.ArrayList;

public class Catalogo {

	private ArrayList<Obres> obras;

	public Catalogo() {
		obras = new ArrayList<Obres>();
	}

	public Catalogo(ArrayList<Obres> obras) {
		this.obras = obras;

	}

	public String addObras(Obres ob) {
		for (int i = 0; i < obras.size(); i++) {
			if (obras.contains(ob)) {
				return "La obra no s'ha pogut afegir";
			}
		}
		obras.add(ob);
		return "La obra s'ha pogut afegir";

	}

	public String removeObras(int ni) {
		for (int i = 0; i < obras.size(); i++) {
			if (obras.contains(ni)) {
				obras.remove(ni);
				return "La obra s'ha pogut eliminar";
			}
		}
		return "La obra no s'ha pogut eliminar";
	}

	public String searchObra(int ni) {
		for (int i = 0; i < obras.size(); i++) {
			if (obras.contains(ni)) {
				return obras.toString();
			}
		}
		return " Aquesta obra no esta";
	}

	public String superficie() {
		double superficie = 0;
		for (int i = 0; i < obras.size(); i++) {
			if (obras.get(i) instanceof Pinturas) {
				Pinturas p = (Pinturas) obras.get(i);
				if (superficie > p.getDimensions()) {
					superficie = p.getDimensions();
				}
			}

			return "Aquesta pintura no existeix";
		}
		return "La pintura mes gran es: " + obras.get((int) superficie);

	}

	public String mesAlta() {
		double altura = 0;
		for (int i = 0; i < obras.size(); i++) {
			if (obras.get(i) instanceof Esculturas) {
				Esculturas e = (Esculturas) obras.get(i);
				if (altura < e.getAltura()) {
					altura = e.getAltura();
				}
			}
			return "Aquesta escultura no existeix";
		}
		return "La altura mes alta es: " + obras.get((int) altura);
	}

}
