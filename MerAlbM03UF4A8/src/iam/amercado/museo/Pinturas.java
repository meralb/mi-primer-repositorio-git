package iam.amercado.museo;

public class Pinturas extends Obres {

	private double dimensions;
	private String soport;

	public Pinturas(String titol, int numinv, Artista autor, int any, double dimensions, String soport) {
		super(titol, numinv, autor, any);
		this.dimensions = dimensions;
		this.soport = soport;
	}

	public double getDimensions() {
		return dimensions;
	}

	public void setDimensions(double dimensions) {
		this.dimensions = dimensions;
	}

	public String getSoport() {
		return soport;
	}

	public void setSoport(String soport) {
		this.soport = soport;
	}

	public String toString(){
		return super.getTitol()+super.getAutor()+super.getAny();
	}
}
