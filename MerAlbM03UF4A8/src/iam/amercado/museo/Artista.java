package iam.amercado.museo;

public class Artista {

	protected String nom;
	protected String llocnaix;

	public Artista(String nom, String llocnaix) {
		this.llocnaix = llocnaix;
		this.nom = nom;
	}

	public Artista() {
		nom = "Artista Anonim";
		llocnaix = "Desconegut";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getLlocnaix() {
		return llocnaix;
	}

	public void setLlocnaix(String llocnaix) {
		this.llocnaix = llocnaix;
	}
	public String toString(){
		
		return getNom();
	}

}
