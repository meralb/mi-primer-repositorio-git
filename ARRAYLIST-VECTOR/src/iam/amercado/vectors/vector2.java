package iam.amercado.vectors;

import java.util.Vector;

public class vector2 {
	public static void main(String[] args) {

		Vector<Double> v1 = new Vector<>();
		int alumnes = (int) (Math.random() * 35 + 1);

		for (int i = 0; i < alumnes; i++) {
			v1.add((Math.random() * 10 + 0));
		}
		
		int[] n = new int[4];
		for (int i = 0; i < n.length; i++) {
			n[i] = 0;
		}

		for (int i = 0; i < v1.size(); i++) {
			if (v1.get(i) >= 0 && v1.get(i) <= 5) {
				n[0]++;
			}
			if (v1.get(i) >= 5 && v1.get(i) <= 7) {
				n[1]++;
			}
			if (v1.get(i) >= 7 && v1.get(i) <= 9) {
				n[2]++;
			} else {
				n[3]++;
			}
		}

		System.out.println("El nombre d'insuficients es: " + n[0]);
		System.out.println("El nombre d'aprovats es: " + n[1]);
		System.out.println("El nombre de notables es: " + n[2]);
		System.out.println("El nombre d'Excel·lents es: " + n[3]);

	}
}
