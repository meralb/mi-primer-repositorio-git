package iam.amercado.vectors;

import java.util.Vector;

public class vector {

	public static void main(String[] args) {

		Vector<Integer> v1 = new Vector<>();

		for (int i = 1; i < 10; i++) {
			v1.addElement(i);
		}

		double modulo = 0;

		for (int i = 0; i < v1.size(); i++) {
			modulo += Math.pow(v1.get(i), 2);
		}
		
		System.out.println("El modul es: "+modulo);
	}
}
