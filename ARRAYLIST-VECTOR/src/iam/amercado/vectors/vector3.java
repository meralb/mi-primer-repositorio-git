package iam.amercado.vectors;

import java.util.Vector;

public class vector3 {

	public static void main(String[] args) {

		Vector<Integer> v1 = new Vector<>();
		Vector<Integer> parells = new Vector<>();
		Vector<Integer> senars = new Vector<>();

		for (int i = 0; i < 20; i++) {
			v1.add(i);
		}

		for (int i = 0; i < v1.size(); i++) {
			if (v1.get(i) % 2 == 0) {
				parells.add(v1.get(i));
			} else {
				senars.add(v1.get(i));
			}

		}

		System.out.println("Els parells son: " + parells);
		System.out.println("Els senars son: " + senars);

	}
}
