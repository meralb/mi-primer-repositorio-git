package iam.amercado.escola;
import java.util.Scanner;
import java.util.Vector;

public class TestClase {

	public static void main(String[] args) {
		
		// demana quantitat d'alumnes
		Scanner scan = new Scanner(System.in);
		 int n;
		do {
			System.out.print("Introdueix la quantitat d'alumnes que vols: ");
			n = scan.nextInt();
		} while (n < 1);
		
		Vector<Estudiant> grupEstudiants = new Vector<Estudiant>();
		
		String nom, cognom, DNI, curs;
		int edat, codiAlumne, nota, codiProfessor;
		
		// Demana dades per a cada alumne
		for (int i = 0; i < n; i++) {
			System.out.println("Alumne num " + i);
			System.out.println("nom: ");
			nom = scan.next();
			
			System.out.println("cognom: ");
			cognom = scan.next();
			
			System.out.println("DNI: ");
			DNI = scan.next();
			
			System.out.println("curs: ");
			curs = scan.next();
			
			System.out.println("edat: ");
			edat = scan.nextInt();
			
			System.out.println("codiAlumne: ");
			codiAlumne = scan.nextInt();
			
			System.out.println("nota: ");
			nota = scan.nextInt();
			
			Estudiant est = new Estudiant(nom, cognom, DNI, curs, edat, codiAlumne,nota);
			grupEstudiants.add(est);
		}
		
		// Demana dades del tutor
		System.out.println("Introdueix les dades del tutor: ");
		System.out.println("nom: ");
		nom = scan.next();
		
		System.out.println("cognom: ");
		cognom = scan.next();
		
		System.out.println("DNI: ");
		DNI = scan.next();
		
		System.out.println("edat: ");
		edat = scan.nextInt();
		
		System.out.println("codiProfessor: ");
		codiProfessor = scan.nextInt();
		
		Professor tutor = new Professor(nom, cognom, DNI, edat, codiProfessor);
		
		Grup grupA = new Grup(grupEstudiants, tutor);
		
		System.out.println(grupA.numAprovats());
		System.out.println(grupA);
		
		scan.close();
	}
}
