package iam.amercado.escola;

import java.util.Iterator;
import java.util.Vector;

//hacer un bucle metiendo datos de cada alumno
public class Grup {
	Vector<Estudiant> estudiants;
	Professor tutor;

	public Grup(Vector<Estudiant> estudiants, Professor tutor) {
		this.estudiants = estudiants;
		this.tutor = tutor;
	}

	public Vector<Estudiant> getEstudiants() {
		return estudiants;
	}

	public int numAprovats() {
		int numAprovats = 0;
		Iterator<Estudiant> it= estudiants.iterator();
		while(it.hasNext()){
			if (it.next().getNota() >= 5)
			numAprovats++;
		}


		return numAprovats;
	}

	public String toString() {
		String estudiantsString = "Els Estudiants son: ";
		
		Iterator<Estudiant> it= estudiants.iterator();
		while(it.hasNext()){
			estudiantsString = it.next().toString();
		}
		
		return estudiantsString + " i el tutor és" + tutor;
	}
}
