package iam.amercado.tenda;

public abstract class Article {

	private int codi;
	private String descripcio;
	private Double preu;
	private int stock;

	public Article(int codi, String descripcio, Double preu, int stock) {
		this.codi = codi;
		this.descripcio = descripcio;
		this.preu = preu;
		this.stock = stock;
	}

	public Double Venta(int quant) {
		Double preuFinal = quant * preu;
		stock -= quant;
		return preuFinal;

	}

	public void Repostar(int quant) {
		this.stock -= quant;

	}

	public int getCodi() {
		return codi;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public Double getPreu() {
		return preu;
	}

	public int getStock() {
		return stock;
	}

	public abstract String MostraContingut();
}
