package iam.amercado.tenda;

public class Cd extends Article {

	private String titol;
	private String nomBanda;
	private int numCancons;

	public Cd(int codi, String descripcio, Double preu, int stock, String titol, String nomBanda, int numCancons) {
		super(codi, descripcio, preu, stock);
		this.titol = titol;
		this.nomBanda = nomBanda;
		this.numCancons = numCancons;
	}

	@Override
	public String MostraContingut() {
		String contingut;
		contingut = "Cd [titol=" + titol + ", codi=" + getCodi() + ", descipcio=" + getDescripcio() + ", preu="
				+ getPreu() + ", stock=" + getStock() + ", nomBanda=" + nomBanda + ", numCancons=" + numCancons + "]";
		return contingut;
	}

}
