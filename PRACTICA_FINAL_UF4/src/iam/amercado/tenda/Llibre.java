package iam.amercado.tenda;

public class Llibre extends Article {

	private String titol;
	private String autor;
	private int numPagines;

	public Llibre(int codi, String descripcio, Double preu, int stock, String titol, String autor, int numPagines) {
		super(codi, descripcio, preu, stock);
		this.titol = titol;
		this.autor = autor;
		this.numPagines = numPagines;
	}

	public String MostraContingut() {
		String contingut;
		contingut = "Llibre [titol=" + titol + ", autor=" + autor + ", codi=" + getCodi() + ", descipcio="
				+ getDescripcio() + ", preu=" + getPreu() + ", stock=" + getStock() + ", numPagines=" + numPagines
				+ "]";
		return contingut;
	}

}
