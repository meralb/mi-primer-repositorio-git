package iam.amercado.tenda;

public class Cancion {

	public int duracio;
	private String nom;

	public Cancion(int duracion, String nom) {
		this.duracio = duracion;
		this.nom = nom;
	}

	public int getDuracion() {
		return duracio;
	}

	public void setDuracion(int duracion) {
		this.duracio = duracion;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}
