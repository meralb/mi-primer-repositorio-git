package iam.amercado.tenda;

public class Dvd extends Article {

	private String titol;
	private String director;
	private Double duracio;
	private String idiomaOriginal;

	public Dvd(int codi, String descripcio, Double preu, int stock, String titol, String director, Double duracio,
			String idiomaOriginal) {
		super(codi, descripcio, preu, stock);
		this.titol = titol;
		this.director = director;
		this.duracio = duracio;
		this.idiomaOriginal = idiomaOriginal;
	}

	public String MostraContingut() {
		String contingut;
		contingut = "Dvd [titol=" + titol + ", codi=" + getCodi() + ", descipcio=" + getDescripcio() + ", preu="
				+ getPreu() + ", stock=" + getStock() + ", director=" + director + ", duracio=" + duracio
				+ ", idiomaOriginal=" + idiomaOriginal + "]";
		return contingut;
	}

}
