package iam.amercado.abstractes;

public class Circle extends Shape {

	protected double radius;

	public Circle() {

	}

	public Circle(double radius) {
		this.radius = radius;
	}

	public Circle(double radius, String color, boolean filled) {
		super(color, filled);
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getArea() {
		double area;
		area=Math.PI*Math.pow(radius, 2);
		return area;
	}

	public double getPerimeter() {
		double perimetre;
		perimetre=Math.PI*radius;
		return perimetre;
	}

	public String toString() {
		return "El radi es: "+radius ;
	}
}
