package iam.solucio.lliga;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.Hashtable;

public class Equip implements Serializable{

	private Hashtable<Integer, Jugador> jugadors = new Hashtable<>();
	private String nomEquip;
	private int puntsLliga = 0;
	public Equip(String fitxerEquips) throws LengthFileException {

		try {
			BufferedReader buferRead = new BufferedReader(new FileReader(fitxerEquips));
			String[] equip = fitxerEquips.split("/");
			nomEquip = equip[1].replace(".txt", "");
			String filaJugador;

			try {
				while ((filaJugador = buferRead.readLine()) != null) {

					Jugador jugador = new Jugador(filaJugador);
					jugadors.put(jugador.getDorsal(), jugador);
				}

				if (jugadors.size() < 15) {
					throw new LengthFileException("El fitxer " + fitxerEquips + " ha de tenir minim15 jugadors.");
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	public void afegirJugador(Jugador j){
		
	}

	public String toString() {
		return "Equip - jugadors=" + jugadors + ", nomEquip=" + nomEquip + ", puntsLliga=" + puntsLliga;
	}

	public String getNom() {
		return nomEquip;
	}

	public int getPunts() {
		return puntsLliga;
	}

	public void incrementaPunts(int punts) {
		this.puntsLliga = punts + puntsLliga;
	}

	public Hashtable<Integer, Jugador> getJugadors() {
		return jugadors;
	}

	public void setJugadors(Hashtable<Integer, Jugador> jugadors) {
		this.jugadors = jugadors;
	}
	
}
