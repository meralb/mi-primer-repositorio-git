package iam.solucio.lliga;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class Lliga implements Serializable{

	private ArrayList<Equip> equipsFinal = new ArrayList<>();
	private Partit[] partitsLliga;
	private FileWriter escriureLliga;
	int numPartits;

	public Lliga(File fitxerLliga) {

		File[] equips = fitxerLliga.listFiles();
		for (File file : equips) {
			if (file != null) {
				Equip equip;
				try {
					equip = new Equip(file.getPath());
					equipsFinal.add(equip);

				} catch (LengthFileException lfe) {
					System.out.println(lfe.getMessage());
				}
			}
		}
		numPartits = equipsFinal.size() * equipsFinal.size() - equipsFinal.size();
		this.partitsLliga = new Partit[numPartits];

		int contPartits = 0;

		for (int i = 0; i < equipsFinal.size(); i++) {
			for (int j = 0; j < equipsFinal.size(); j++) {
				if (j != i) {
					partitsLliga[contPartits++] = new Partit(equipsFinal.get(i), equipsFinal.get(j));
				} else {

				}
			}
		}

		try {
			escriureLliga = new FileWriter("ResultatLliga.txt", true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Partit[] jugaPartit(int a) {
		int randGols = (int) (Math.random() * 5 + 0);
		int randGols2 = (int) (Math.random() * 5 + 0);

		for (int i = 0; i < randGols; i++) {
			partitsLliga[a].marcaLocal();
		}
		for (int i = 0; i < randGols2; i++) {
			partitsLliga[a].marcaVisitant();
		}
		partitsLliga[a].fin();

		return partitsLliga;
	}

	@Override
	public String toString() {
		return "Lliga - Equips= " + equipsFinal + ", partits=" + Arrays.toString(partitsLliga);
	}

	public void jugaLliga() {

		for (int i = 0; i < partitsLliga.length; i++) {
			jugaPartit(i);
			try {
				escriureLliga.write(partitsLliga[i].toString() + "\n");
				escriureLliga.write(partitsLliga[i].getArrayGols() + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			escriureLliga.write(classificacio());
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			escriureLliga.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String classificacio() {
		String puntuacio = "\nTaula classificacio de la lliga: \n";
		for (int i = 0; i < equipsFinal.size(); i++) {
			puntuacio += equipsFinal.get(i).getNom() + " - " + equipsFinal.get(i).getPunts() + " punts\n";
		}
		return puntuacio;
	}

	public ArrayList<Equip> getEquips() {
		return equipsFinal;
	}

	public void setEquips(ArrayList<Equip> equips) {
		this.equipsFinal = equips;
	}

	public Partit[] getPartits() {
		return partitsLliga;
	}

	public void setPartits(Partit[] partits) {
		this.partitsLliga = partits;
	}

}
