package iam.solucio.lliga;

import java.io.Serializable;

public class Jugador implements Serializable{

	private int dorsal;
	private transient int altura;
	private String nom, cognom;

	public Jugador(){
		
	}
	public Jugador(String linea) {

		String[] dadesJugador = linea.split("#");
		this.dorsal = Integer.parseInt(dadesJugador[0]);
		this.cognom = dadesJugador[1];
		this.nom = dadesJugador[2];
		this.altura = Integer.parseInt(dadesJugador[3]);
	}

	public String toString() {
		return "Jugador [dorsal=" + dorsal + ", altura=" + altura + ", nom=" + nom + ", cognom=" + cognom + "]";
	}

	public int getDorsal() {
		return dorsal;
	}

	public void setDorsal(int dorsal) {
		this.dorsal = dorsal;
	}

	public int getAltura() {
		return altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognom() {
		return cognom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

}
