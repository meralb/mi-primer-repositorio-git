package iam.solucio.lliga;

import java.io.Serializable;
import java.util.ArrayList;

public class Partit implements Serializable{

	private Equip equipLocal, equipVisitant;
	private int golsEquipLocal;
	private int golsEquipVisitant;
	private ArrayList<Gol> arrayGols = new ArrayList<>();

	public Partit(Equip equipLocal, Equip equipVisitant) {
		this.equipLocal = equipLocal;
		this.equipVisitant = equipVisitant;
		golsEquipLocal = 0;
		golsEquipVisitant = 0;
	}

	public void marcaLocal() {
		golsEquipLocal++;
		int minutGol = (int) (Math.random() * (90 - 1) + 1);
		int jugadorGol = (int) (Math.random() * (18 - 1) + 1);
		Jugador jugador = equipLocal.getJugadors().get(jugadorGol);
		Gol gol = new Gol(minutGol, jugador.getNom(), jugador.getCognom());
		arrayGols.add(gol);
	}

	public void marcaVisitant() {
		golsEquipVisitant++;
		int minutGol = (int) (Math.random() * (90 - 1) + 1);
		int jugadorGol = (int) (Math.random() * (18 - 1) + 1);
		Jugador jugador = equipLocal.getJugadors().get(jugadorGol);
		Gol gol = new Gol(minutGol, jugador.getNom(), jugador.getCognom());
		arrayGols.add(gol);
	}

	public String marcador() {

		return golsEquipLocal + equipLocal.getNom() + " - " + equipVisitant.getNom() + golsEquipVisitant;
	}

	public String fin() {
		String guanyador;

		if (golsEquipLocal > golsEquipVisitant) {
			guanyador = ("Guanya l'equip: " + equipLocal.getNom());
			equipLocal.incrementaPunts(3);

		} else if (golsEquipLocal < golsEquipVisitant) {
			guanyador = ("Guanya l'equip: " + equipVisitant.getNom());
			equipVisitant.incrementaPunts(3);

		} else {
			guanyador = ("Empat entre l'equip " + equipLocal.getNom() + " i l'equip " + equipVisitant.getNom());
			equipLocal.incrementaPunts(1);
			equipVisitant.incrementaPunts(1);

		}

		return guanyador;
	}

	@Override
	public String toString() {
		return "Partit: " + equipLocal.getNom() + "(" + golsEquipLocal + ")" + " -- " + equipVisitant.getNom() + "("
				+ golsEquipVisitant + ")\n";
	}

	public Equip getEquipLocal() {
		return equipLocal;
	}

	public void setEquipLocal(Equip equipLocal) {
		this.equipLocal = equipLocal;
	}

	public Equip getEquipVisitant() {
		return equipVisitant;
	}

	public void setEquipVisitant(Equip equipVisitant) {
		this.equipVisitant = equipVisitant;
	}

	public int getGolsEquipLocal() {
		return golsEquipLocal;
	}

	public void setGolsEquipLocal(int golsEquipLocal) {
		this.golsEquipLocal = golsEquipLocal;
	}

	public int getGolsEquipVisitant() {
		return golsEquipVisitant;
	}

	public void setGolsEquipVisitant(int golsEquipVisitant) {
		this.golsEquipVisitant = golsEquipVisitant;
	}

	public ArrayList<Gol> getArrayGols() {
		return arrayGols;
	}

	public void setArrayGols(ArrayList<Gol> arrayGols) {
		this.arrayGols = arrayGols;
	}

}