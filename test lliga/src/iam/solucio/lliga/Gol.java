package iam.solucio.lliga;

import java.io.Serializable;

public class Gol implements Serializable{

	private int minut;
	private String nomJugador;
	private String cognomJugador;

	public Gol(int temps, String nomJugador, String cognom) {
		this.minut = temps;
		this.nomJugador = nomJugador;
		this.cognomJugador = cognom;
	}

	public int getMinut() {
		return minut;
	}

	public void setMinut(int minut) {
		this.minut = minut;
	}

	public String getNomJugador() {
		return nomJugador;
	}

	public void setNomJugador(String nomJugador) {
		this.nomJugador = nomJugador;
	}

	public String getCognomJugador() {
		return cognomJugador;
	}

	public void setCognomJugador(String cognomJugador) {
		this.cognomJugador = cognomJugador;
	}

	@Override
	public String toString() {
		return "Gol - minut=" + minut + ", Jugador=" + nomJugador + " " + cognomJugador;
	}

}
