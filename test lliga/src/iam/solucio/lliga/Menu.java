package iam.solucio.lliga;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

public class Menu {
	private int opcio;
	
	String fitxerEquips;
	String filaJugador;
	Jugador nuevoJugador;

	public Menu() {

	}

	public int doMenu() {

		int opcio;
		Scanner scann = new Scanner(System.in);

		do {
			System.out.println("------Lliga de futbol Profesional------");
			System.out.println("1)Gestionar Equips");
			System.out.println("2)Jugar Lliga");
			System.out.println("3)Sortir");

			System.out.println("Escull una opcio");
			opcio = Integer.parseInt(scann.nextLine());

			if (opcio < 1 || opcio > 3) {
				System.out.println("Opcio Incorrecte");
			}
		} while (opcio < 1 || opcio > 3);

		return opcio;
	}

	public int gestionarEquips() {
		int opcio;
		Scanner scann = new Scanner(System.in);

		do {
			System.out.println("------Gestionar Equips------");
			System.out.println("1)Donar d'alta Equip");
			System.out.println("2)Donar de baixa Equip");
			System.out.println("3)Modificar Equip");
			System.out.println("4)Gestionar Jugador");
			System.out.println("5)Sortir");

			System.out.println("Escull una opcio");
			opcio = Integer.parseInt(scann.nextLine());

			if (opcio < 1 || opcio > 5) {
				System.out.println("Opcio Incorrecte");
			}

		} while (opcio < 1 || opcio > 5);

		return opcio;
	}

	public int gestionarJugador() {
		int opcio;
		Scanner scann = new Scanner(System.in);

		do {
			System.out.println("------Gestionar Jugadors------");
			System.out.println("1)Donar d'alta jugador");
			System.out.println("2)Donar de baixa jugador");
			System.out.println("3)Modificar jugador");
			System.out.println("4)Sortir");

			System.out.println("Escull una opcio");
			opcio = Integer.parseInt(scann.nextLine());

			if (opcio < 1 || opcio > 4) {
				System.out.println("Opcio Incorrecte");
			}

		} while (opcio < 1 || opcio > 4);
		return opcio;
	}

	public boolean altaEquip(ArrayList<Equip> equipos) {
		Scanner scann = new Scanner(System.in);
		System.out.println("Introduzca el nombre del equipo: ");
		String nomEquipo = scann.next();

		// Nos aseguramos de que el quipo no este dado de alta
		for (int i = 0; i < equipos.size(); i++) {
			if (equipos.get(i).getNom().equals(nomEquipo))
				return false;
		}

		Hashtable<Integer, Jugador> jugadores = new Hashtable<Integer, Jugador>();
		Jugador j = new Jugador();
		int plantilla = 0;

		// Nos aseguramos de que la plantilla contenga como minimo 15 jugadores
		while (plantilla < 15) {
			System.out.println("Numero de integrantes en la plantilla: ");
			plantilla = scann.nextInt();
			if (plantilla < 15)
				System.out.println("El numero minimo de jugadores es de 3");
		}

		for (int i = 0; i < plantilla; i++) {
			nuevoJugador = new Jugador();
			System.out.println("Nombre: ");
			String nombre = scann.next();
			System.out.println("Apellido: ");
			String apellido = scann.next();
			System.out.println("Edad: ");
			int edad = scann.nextInt();
			System.out.println("Altura: ");
			double altura = scann.nextDouble();
			System.out.println("Peso: ");
			double peso = scann.nextDouble();
			System.out.println("Dorsal: ");
			int dorsal = scann.nextInt();
			nuevoJugador = new Jugador(filaJugador);
		}
		jugadores.put(nuevoJugador.getDorsal(), nuevoJugador);

		// Nos aseguramos de que no halla duplicado en los dorsales
		if (jugadores.size() < 3)
			return false;

		Equip equipo;
		try {
			equipo = new Equip(fitxerEquips);
			equipos.add(equipo);
		} catch (LengthFileException e) {
			e.printStackTrace();
		}

		return true;
	}

	public boolean baixaEquip(ArrayList<Equip> equips) {

		Scanner scann = new Scanner(System.in);

		String nomEquip;
		System.out.println("Equipo: ");
		nomEquip = scann.next();

		for (int i = 0; i < equips.size(); i++) {
			if (equips.get(i).getNom().equals(nomEquip)) {
				equips.remove(equips.get(i));
				return true;
			}
		}
		return false;
	}

}
