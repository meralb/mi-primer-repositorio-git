package meralbgrup;

public class Alumn {
	private String Nom,Cognom;
	private String Tutor;
	private float Nota;
	
	
	public Alumn (String Nom,String Cognom, float Nota){
		this.Nom = Nom;
		this.Cognom = Cognom;
		this.Nota = Nota;
	}
	
	public String mostraNom(){
		return this.Nom;
	}
	
	public String mostraCognom(){
		return this.Cognom;
	}
	
	public float mostraNota(){
		return this.Nota;
	}
	
	public String mostraNomComplert(){
		String nomcompl = (this.Nom+" "+this.Cognom);
		return nomcompl;
	}
	
	public void setTutor(String Tutor){
		this.Tutor=Tutor;
	}
	
	public String toString(){
		String tostring=mostraNomComplert();
		return tostring;
	}

}
