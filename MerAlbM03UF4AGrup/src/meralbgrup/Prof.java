package meralbgrup;

public class Prof {
	private String Nom,Cognom;
	
	public Prof (String Nom,String Cognom){
		this.Nom = Nom;
		this.Cognom = Cognom;
	}
	
	public String mostraNom(){
		return this.Nom;
	}
	
	public String mostraCognom(){
		return this.Cognom;
	}
	
	
	public String mostraNomComplert(){
		String nomcompl = (this.Nom+" "+this.Cognom);
		return nomcompl;
	}
	
}
