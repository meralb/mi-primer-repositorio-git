package MerAlbM03UF4AGRUP;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		int numeroAlumnes;

		do {
			System.out.println("Introdueix la quantitat d'alumnes que vulguis guardar: ");
			numeroAlumnes = input.nextInt();

			if (numeroAlumnes < 1)
				System.out.println("Ep! No pot haver menys d'un alumne");

		} while (numeroAlumnes < 1);

		Alumne al1 = new Alumne("Albert", "Mercado Aznar", 8);

		Grup gp1 = new Grup(numeroAlumnes);
		gp1.addAlumne(al1);
		gp1.setProfessor("Salvador Campo Manzarico");

	}
}
