package MerAlbM03UF4AGRUP;

public class Profesor {

	private String nom, cognoms;

	public Profesor(String nom, String cognoms) {

		this.nom = nom;
		this.cognoms = cognoms;

	}

	public String mostraNomProfesor() {

		String mostraNomP;
		mostraNomP = (nom + " " + cognoms);
		return mostraNomP;
	}

}
