package MerAlbM03UF4AGRUP;

public class Grup {

	private int numAlum;
	Alumne listAlumn[];

	public Grup(int numeroAlumnes) {

		this.numAlum = numeroAlumnes;
		this.listAlumn = new Alumne[numeroAlumnes];
	}

	public void addAlumne(Alumne alum) {
		listAlumn[numAlum++] = alum;
	}

	public void setProfessor(String professor) {
		for (int i = 0; i < numAlum; i++) {
			listAlumn[i].setProfessor(professor);
		}
	}

	public int numAprovats() {
		int aprovats = 0;
		for (int i = 0; i < numAlum; i++) {
			if (listAlumn[i].getNota() > 5) {
				aprovats++;
			}
		}
		return aprovats;
	}
}
