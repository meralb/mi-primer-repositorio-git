package MerAlbM03UF4AGRUP;

public class Alumne {

	private String nom, cognoms, professor;
	private double nota;

	public Alumne(String nom, String cognoms, double nota) {

		this.nom = nom;
		this.cognoms = cognoms;
		this.nota = nota;
		professor = null;
	}

	public Alumne(String nom, String cognoms, String professor, double nota) {

		this.nom = nom;
		this.cognoms = cognoms;
		this.professor = professor;
		this.nota = nota;
	}

	public void setProfessor(String profe) {
		this.professor = profe;
	}

	public String toString() {
		return ("L'alumne " + this.nom + " " + this.cognoms + " ha tret una nota de: " + this.nota + " "
				+ "amb el professor" + this.professor);

	}

	public double getNota() {
		return this.nota;
	}

	public String getNom() {
		return nom;
	}

	public String getCognoms() {
		return cognoms;
	}

}
