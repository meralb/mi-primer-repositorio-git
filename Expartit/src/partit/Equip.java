package partit;

public class Equip {
	private String nomEquip;
	private int puntsLliga;
	
	
	public Equip(String nomEquip) {
		
		this.nomEquip = nomEquip;
		this.puntsLliga =0;
	}


	public String getNom() {
		return nomEquip;
	}


	public int getPunts() {
		
		return puntsLliga;
	}
	
	public int incrementaPunts (int punts){
		puntsLliga =punts+puntsLliga;
		System.out.println(puntsLliga);
		return puntsLliga;
	}
	
	
	
	
}
