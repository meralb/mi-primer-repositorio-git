package partit;

public class Partit {
	private Equip equip1,equip2;
	private int golsEquip1,golsEquip2;
	
	
	public Partit(String nomEquip1, String nomEquip2) {
	
		equip1= new Equip(nomEquip1);
		equip2= new Equip(nomEquip2);
		golsEquip1=0;
		golsEquip2=0;
	}


	public Partit(String nomEquip1, String nomEquip2, int golsEquip1, int golsEquip2) {
		equip1= new Equip(nomEquip1);
		equip2= new Equip(nomEquip2);
		this.golsEquip1 = golsEquip1;
		this.golsEquip2 = golsEquip2;
	}
	
	
	
	public void marca1(){
		golsEquip1++;
		
	}
	
	public void marca2(){
		golsEquip2++;
		
	}
	public void marcador(){
		System.out.println(equip1.getNom()+": "+golsEquip1+" "+equip2.getNom()+": "+golsEquip2);
	}
	public void fi(){
		if (golsEquip1>golsEquip2) {
			System.out.println(equip1.getNom());
			equip1.incrementaPunts(3);
			
		}else if(golsEquip1<golsEquip2){
			System.out.println(equip2.getNom());
			equip2.incrementaPunts(3);
		}else{
			System.out.println("Empat");
			equip2.incrementaPunts(1);
			equip1.incrementaPunts(1);
		}
	}


	public String getNomEquip1() {
		String nom = equip1.getNom();
		return nom;
	}
	
	public String getNomEquip2() {
		String nom = equip2.getNom();
		return nom;
	}

	
	public int getGolsEquip1() {
		return golsEquip1;
	}
	public void setGolsEquip1(int golsEquip1) {
		this.golsEquip1 = golsEquip1;
	}
	public int getGolsEquip2() {
		return golsEquip2;
	}
	public void setGolsEquip2(int golsEquip2) {
		this.golsEquip2 = golsEquip2;
	}


	@Override
	public String toString() {
	String tostring=("Partit nomEquip1=" + equip1.getNom() + ", 		nomEquip2=" + equip2.getNom() + ", golsEquip1=" + golsEquip1
	+ ", golsEquip2=" + golsEquip2)
		return tostring;
	}
	
}
