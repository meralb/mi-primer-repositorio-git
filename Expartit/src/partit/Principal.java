package partit;

public class Principal {

	public static void main(String[] args) {
		Partit p1 = new Partit("equip1", "equip2");
		Partit p2 = new Partit("equip3", "equip4", 1, 1);
		
		p1.marca2();
		p1.marca1();
		p1.marca2();
		p1.marcador();
		p2.marcador();
		p1.fi();
		p2.fi();

	}

}
