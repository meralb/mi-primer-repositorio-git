package iam.amercado.vectorsComparables;

public class OrdenaVector {

	public static void directSort(Ordenable v[]) {
		// Codi per ordenar
		int i = 0, n = 0, j = 0;
		Ordenable aux;
		while (i < n - 1) {
			j = i + 1;
			while (j < n) {
				if (v[j].menorQue(v[i])) {
					aux = v[i];
					v[i] = v[j];
					v[j] = aux;
				}
				j++;
			}
			i++;
		}

	}

	public static void imprimirVector(Imprimible v[]) {
		// Codi per imprimir el vector
		for (int i = 0; i < v.length; i++) {
			if (v[i] instanceof Persona) {
				System.out.println(v[i]);
			}

		}

	}

}
