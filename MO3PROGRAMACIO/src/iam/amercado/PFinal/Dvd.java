package iam.amercado.PFinal;

public class Dvd extends Article {
	private String titol, director, idioma;
	private int duracio;

	public Dvd(int codi, int stock, double preu, String descripcio,
			String titol, String idioma, String director, int duracio) {
		super(codi, stock, preu, descripcio);
		this.titol = titol;
		this.idioma = idioma;
		this.director = director;
		this.duracio = duracio;
	}

	public String getTitulo() {
		return titol;
	}

	public void setTitulo(String titol) {
		this.titol = titol;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public int getDuracion() {
		return duracio;
	}

	public void setDuracion(int duracio) {
		this.duracio = duracio;
	}

	
	@Override
	public String Mostra() {
		return "Dvd - Director - " + director + "\n\tTitol - " + titol + "\n\tIdioma - "
				+ idioma + "\n\tDuracio - " + duracio+" segons";
	}

	@Override
	public int compareTo(Article o) {
		if (this.getCodi() > o.getCodi())
			return -1;
		return 1;

	}

}
