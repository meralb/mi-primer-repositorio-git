package iam.amercado.PFinal;

public abstract class Article implements Comparable<Article> {
	protected int codi;
	protected int stock;
	protected double preu;
	protected String descripcio;

	public Article(int codi, int stock, double preu, String descripcio) {
		this.codi = codi;
		this.stock = stock;
		this.preu = preu;
		this.descripcio = descripcio;
	}

	public int getCodi() {
		return codi;
	}

	public void setCodi(int codi) {
		this.codi = codi;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public double getPreu() {
		return preu;
	}

	public void setPreu(double preu) {
		this.preu = preu;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public double Venta(int unitats){
		if (stock >= unitats) {
			stock=stock-unitats;
			return preu * unitats; 
		}
		else {
		return -1;}
	};
	
	public void Repostar(int unitats){
		stock+=unitats;	
	};
	
	public abstract String Mostra ();
	
	
}
