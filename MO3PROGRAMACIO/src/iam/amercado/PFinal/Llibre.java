package iam.amercado.PFinal;

public class Llibre extends Article {

	private String titol, autor;
	private int numPagines;

	public Llibre(int codi, int stock, double preu, String descripcio, String titol, String autor, int numPagines) {
		super(codi, stock, preu, descripcio);
		this.titol = titol;
		this.autor = autor;
		this.numPagines = numPagines;
	}

	public String getTitol() {
		return titol;
	}

	public void setTitul(String titol) {
		this.titol = titol;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getPagines() {
		return numPagines;
	}

	public void setPaginas(int numPagines) {
		this.numPagines = numPagines;
	}

	@Override
	public String Mostra() {
		return "Llibre - Autor - " + autor + " \n\tTitol - " + titol + "\n\tPagines - " + numPagines;
	}

	@Override
	public int compareTo(Article o) {
		if (this.getCodi() > o.getCodi())
			return -1;
		return 1;

	}

}
