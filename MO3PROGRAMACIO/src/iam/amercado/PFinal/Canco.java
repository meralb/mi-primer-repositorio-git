package iam.amercado.PFinal;

public class Canco {

		public double duracio;
		private String nom;
		
		public Canco(double duracio,String nom){
			this.duracio=duracio;
			this.nom=nom;
		}

		public double getDuracio() {
			return duracio;
		}

		public void setDuracio(int duracio) {
			this.duracio = duracio;
		}

		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}
		public String toString (){
			return "\n\t"+nom+ " Duracio: "+ duracio;
		}
		
		
}
