package iam.amercado.taylor;

public class Taylor {

	public static double sin(double x) {
		double sin;

		sin = x - Math.pow(x, 3) / 3 + Math.pow(x, 5) / 120;

		return sin;
	}

	public static double cos(double x) {
		double cos;

		cos = 1 - Math.pow(x, 2) / 2 + Math.pow(x, 4) / 24;

		return cos;
	}

	public static double exp(double x) {
		double exp;

		exp = 1 + x + Math.pow(x, 2) / 2 + Math.pow(x, 3) / 6 + Math.pow(x, 4) / 24 + Math.pow(x, 5) / 120;

		return exp;
	}

	public static String author() {
		return ("Nom:Albert Mercado \n" + "Edat: 20 anys\n");
	}

}
