package iam.amercado.exception;

public class PrimerCoeficientZeroException extends Exception {

	public PrimerCoeficientZeroException() {
	}

	public PrimerCoeficientZeroException(String message){
		message="El primer coeficient de l'equacio es 0";
	}
}
