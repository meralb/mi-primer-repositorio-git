package iam.amercado.exception;

import java.util.Scanner;

public class Principal {
	static Scanner teclado = new Scanner(System.in);

	public static void main(String[] args) {

		System.out.println("Introdueix el valor de la a: ");
		double a = teclado.nextDouble();
		System.out.println("Introdueix el valor de la b: ");
		double b = teclado.nextDouble();
		System.out.println("Introdueix el valor de la c: ");
		double c = teclado.nextDouble();
		EquacioGrau2 eq2 = new EquacioGrau2(a, b, c);

		if (b * b < 4 * a * c) {
			eq2.arrels();
			System.out.println("Solucion perteneciente al campo de los numeros complejos.");
		} else if (a != 0 && b * b > 4 * a * c) {
			System.out.println("La primera solucion es " + eq2.getSol1());
			System.out.println("La segunda solucion es " + eq2.getSol2());
		} else {
			System.out.println("Esta ecuacion no es de segundo grado.");
		}

	}
}
