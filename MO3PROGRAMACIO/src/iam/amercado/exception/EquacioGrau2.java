package iam.amercado.exception;

/*a equacio de segon grau s'escriu com:

  a xÂ² + b x + c = 0

on a, b i c sÃ³n nombres reals que ens donen

i les solucions de l'equaciÃ³ sÃ³n

  x = ( -b + sqrt(bÂ² -4 a c) ) / (2 a)

  x = ( -b - sqrt(bÂ² -4 a c) ) / (2 a)

A aquest exercici heu d'implementar:

*/
public class EquacioGrau2 {
	private double a, b, c;
	private double sol1, sol2;

	public EquacioGrau2(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public double getSol1() {
		return sol1;
	}

	public double getSol2() {
		return sol2;
	}

	public void arrels() {
		try {
			sol1 = (-b + Math.sqrt((b * b) - (4 * a * c)) / (2 * a));
			sol2 = (-b - Math.sqrt((b * b) - (4 * a * c)) / (2 * a));
			if (a == 0) {
				throw new PrimerCoeficientZeroException();
			}

			if (b * b < 4 * a * c) {
				
				throw new NoArrelsRealsException();
			}
			

		} catch (PrimerCoeficientZeroException e) {
			System.out.println(e.getMessage());

		} catch (NoArrelsRealsException e) {
			System.out.println(e.getMessage());
		}
	}
}

/*
 * (1) Una classe anomenada â€œEquacioGrau2â€� que al constructor rep els termes a,
 * b i c de l'equaciÃ³.
 * 
 * - tindrÃ  uns atributs sol1 i sol2 on guardar les solucions.
 * 
 * - tindrÃ  un mÃ¨tode arrels() que intenta trobar les solucions i les guarda als
 * atributs sol1 i sol2. Si no pot trobar les solucions per que a == 0 llavors
 * llenÃ§a una excepciÃ³ del tipus PrimerCoeficientZeroException. Si no pot trobar
 * les solucions per que b*b < 4*a*c llavors llenÃ§a una excepciÃ³ del tipus
 * NoArrelsRealsException
 * 
 * - tindrÃ  uns mÃ¨todes getSol1() i getSol2() que retornen els valors dels
 * atributs sol1 i sol2, respectivament.
 * 
 * 
 * 
 * (2) Un programa principal que demani per teclat els termes d'una equaciÃ³ de
 * segon grau i crei un objecte de la classe â€œEquacioGrau2â€� per resoldre-la. El
 * programa principal ha de gestionar qualsevol excepciÃ³ que es pugui produir.
 */