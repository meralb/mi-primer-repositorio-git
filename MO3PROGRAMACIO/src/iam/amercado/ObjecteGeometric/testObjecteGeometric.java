package iam.amercado.ObjecteGeometric;

public class testObjecteGeometric {

	public static void main(String[] args) {
		
		//creacio de cercles
		
		Cercle c1= new Cercle(5,5,6,"Negre");
		Cercle c2= new Cercle(3,4,7);
		Cercle c3= new Cercle(5,"Groc");
		Cercle c4= new Cercle(5);
		
		//creacio de quadrats
		Quadrat q1= new Quadrat("Verd",2);
		Quadrat q2= new Quadrat("Blau");
		Quadrat q3= new Quadrat(5);



		
		System.out.println(c1.toString());
		System.out.println(c1.perimetre());
		System.out.println(c2.perimetre());
		System.out.println(c3.perimetre());
		System.out.println(c4.perimetre());

		System.out.println(c1.area());
		System.out.println(c2.area());
		System.out.println(c3.area());
		System.out.println(c4.area());

		System.out.println(q1.perimetre());
		System.out.println(q2.perimetre());
		System.out.println(q3.perimetre());

		
		System.out.println(q1.area());
		System.out.println(q2.area());
		System.out.println(q3.area());



	}

}
