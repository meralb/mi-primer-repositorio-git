package iam.amercado.ObjecteGeometric;

public class Quadrat extends ObjecteGeometric {
	private double costat;

	public Quadrat(String color,double costat) {
		super(color);
		this.costat = costat;
	}

	public Quadrat(String color) {
		super(color);
	}

	public Quadrat(double costat) {
		this.costat = costat;
	}
	

	public double perimetre() {
		double perimetre;
		perimetre = 4 * costat;
		return perimetre;
	}

	public double area() {
		double area;
		area = costat * costat;
		return area;
	}
	
	public String toString(){
		return "costat:";
	}
}
