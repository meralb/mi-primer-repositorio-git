package iam.amercado.ObjecteGeometric;

public class Cercle extends ObjecteGeometric{

	private double radi;
	
	public Cercle(double Radi,int x,int y, String color){
		super(x,y,color);
		this.radi=radi;
	}
	
	public Cercle(double radi, int x,int y){
		super(x,y);
		this.radi=radi;
	}
	
	public Cercle(double radi, String color){
		super(color);
		this.radi=radi;
	}
	
	public Cercle(double radi){
		this.radi=radi;
	}
	
	public Cercle(String color){
		super(color);
		this.radi=5;
	}
	
	public double perimetre(){
		double perimetre;
		perimetre=Math.PI*2*radi;
		return perimetre;
	}
	
	public double area(){
		double area;
		area=Math.PI*radi*radi;
		return area;
	}
	
	public String toString(){
		return "radi: "+radi+ "color: "+ super.color;
	}
}
