package iam.amercado.ObjecteGeometric;

public class ObjecteGeometric {

	private int x, y;
	protected String color;

	public ObjecteGeometric(int y, int x, String color) {
		this.y = y;
		this.x = x;
		this.color = color;
	}

	public ObjecteGeometric(int y, int x) {
		this.y = 1;
		this.x = 2;
		this.color = "Blau";
	}

	public ObjecteGeometric(String color) {
		this.y = 1;
		this.x = 2;
		this.color = color;
	}

	public ObjecteGeometric() {
		this.color = "Taronja";
		this.x = 2;
		this.y = 6;
	}

	public String toString() {
		return "Valor de x:"+ x +"Valor de y:"+ y + "Color: "+ color;
	}
}
