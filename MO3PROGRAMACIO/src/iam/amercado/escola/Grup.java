package iam.amercado.escola;

import java.io.Serializable;

//hacer un bucle metiendo datos de cada alumno
public class Grup implements Serializable{
	private Estudiant[] estudiants;
	private Professor tutor;

	public Grup(Estudiant[] estudiants, Professor tutor) {
		this.estudiants = estudiants;
		this.tutor = tutor;
	}

	public Estudiant[] getEstudiants() {
		return estudiants;
	}

	public int numAprovats() {
		int numAprovats = 0;
		for (int i = 0; i < estudiants.length; i++) {
			if (estudiants[i].getNota() >= 5)
				numAprovats++;
		}

		return numAprovats;
	}

	public String toString() {
		String estudiantsString = "Els Estudiants son: ";
		for (int i = 0; i < estudiants.length; i++) {
			estudiantsString = estudiantsString + estudiants[i] + " ";
		}

		return estudiantsString + " i el tutor és" + tutor;
	}
}
