package iam.amercado.escola;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class TestClase {

	public static void main(String[] args) {

		// demana quantitat d'alumnes
		Scanner scan = new Scanner(System.in);
		int n;
		do {
			System.out.print("Introdueix la quantitat d'alumnes que vols: ");
			n = scan.nextInt();
		} while (n < 1);

		Estudiant[] grupEstudiants = new Estudiant[n];

		String nom, cognom, DNI, curs;
		int edat, codiAlumne, nota, codiProfessor;

		// Demana dades per a cada alumne
		for (int i = 0; i < n; i++) {
			System.out.println("Alumne num " + i);
			System.out.println("nom: ");
			nom = scan.next();

			System.out.println("cognom: ");
			cognom = scan.next();

			System.out.println("DNI: ");
			DNI = scan.next();

			System.out.println("curs: ");
			curs = scan.next();

			System.out.println("edat: ");
			edat = scan.nextInt();

			System.out.println("codiAlumne: ");
			codiAlumne = scan.nextInt();

			System.out.println("nota: ");
			nota = scan.nextInt();

			Estudiant est = new Estudiant(nom, cognom, DNI, curs, edat, codiAlumne, nota);
			grupEstudiants[i] = est;
		}

		// Demana dades del tutor
		System.out.println("Introdueix les dades del tutor: ");
		System.out.println("nom: ");
		nom = scan.next();

		System.out.println("cognom: ");
		cognom = scan.next();

		System.out.println("DNI: ");
		DNI = scan.next();

		System.out.println("edat: ");
		edat = scan.nextInt();

		System.out.println("codiProfessor: ");
		codiProfessor = scan.nextInt();

		Professor tutor = new Professor(nom, cognom, DNI, edat, codiProfessor);

		Grup grupA = new Grup(grupEstudiants, tutor);

		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("grupA1"));
			out.writeObject(grupA);
			out.close();

			for (int j = 0; j < grupEstudiants.length; j++) {
				ObjectOutputStream outEstudiants = new ObjectOutputStream(
						new FileOutputStream("Esrtudiants" + (j + 1)));
				outEstudiants.writeObject(grupEstudiants[j]);
				outEstudiants.close();
			}

			Grup a2;
			ObjectInputStream in;
			in = new ObjectInputStream(new FileInputStream("grupA1"));
			a2 = (Grup) in.readObject();
			in.close();
			System.out.println(a2.toString());

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();

		}

		System.out.println(grupA.numAprovats());
		System.out.println(grupA);

		scan.close();
	}
}
