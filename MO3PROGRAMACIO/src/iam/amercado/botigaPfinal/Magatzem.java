package iam.amercado.botigaPfinal;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Magatzem {
	private Article[] articles;

	public Magatzem(Article[] articles) {
		this.articles = articles;
	}

	public void buscarArticle(int codi) {
		for (int i = 0; i < articles.length; i++) {
			if (articles[i].getCodi() == codi) {
				articles[i].toString();
				break;
			} else {
				System.out.println("No tenim cap article amb aquest codi.\n");
			}
		}
	}

	public void senseStock() {
		for (int i = 0; i < articles.length; i++) {
			if (articles[i].getStock() == 0) {
				System.out.println(articles[i].toString());
			}
		}
	}

	public String getInfo() {
		String info = null;
		for (int i = 0; i < articles.length; i++) {
			info += (articles[i].toString());
		}
		return info;
	}

	public String getInfoSort() {
		String infoSort = "";
		Arrays.sort(articles);

		for (int i = 0; i < articles.length; i++) {
			infoSort = infoSort + articles[i].toString();
		}
		return infoSort;
	}

}
