package iam.amercado.botigaPfinal;

import java.util.ArrayList;

public class Cd extends Article {

	private String titol, banda;
	private int nCanco;
	private int num;
	private ArrayList<Canco> cancons;

	public Cd(int codi, int stock, double preu, String descripcio, String titol, String banda, int nCanco,
			ArrayList<Canco> cancons) {
		super(codi, stock, preu, descripcio);
		this.titol = titol;
		this.banda = banda;
		this.nCanco = nCanco;
		this.cancons = cancons;
	}

	public String getTitol() {
		return titol;
	}

	public void setTitol(String titol) {
		this.titol = titol;
	}

	public String getBanda() {
		return banda;
	}

	public void setBanda(String banda) {
		this.banda = banda;
	}

	public int getnCanco() {
		return nCanco;
	}

	public void setnCanco(int nCanco) {
		this.nCanco = nCanco;
	}

	public ArrayList<Canco> getcancons() {
		return cancons;
	}

	public void setcancons(ArrayList<Canco> cancons) {
		this.cancons = cancons;
	}

	@Override
	public String Mostra() {
		return "Cd - Banda = " + banda + ", \n\tNom " + cancons.toString();
	}

	@Override
	public int compareTo(Article o) {
		if (this.getCodi() > o.getCodi())
			return -1;
		return 1;

	}

}
