package iam.amercado.lliga;

import java.util.Scanner;

public class TestLliga {

	public static void main(String[] args) {
		Lliga lliga;
		Equip[] equips;
		Scanner sca = new Scanner(System.in);
		int numEquips;
		
		System.out.println("Quantitat d'equips: ");
		numEquips = sca.nextInt();
		equips = new Equip[numEquips];
		
		for(int i=0; i<numEquips;i++){
			String nom;
			System.out.println("Introdueix el nom de l'equip "+i);
			nom = sca.next();
			equips[i] = new Equip(nom);
		}
		
		lliga = new Lliga(equips);
		
		lliga.jugarLliga();

		System.out.println(lliga.classificacio());

		sca.close();
	}



}
