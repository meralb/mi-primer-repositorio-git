package iam.amercado.lliga;


public class Equip implements Comparable<Equip>{
	String nomEquip;
	int puntsLliga;
	static int numEquips;
	
	public Equip(String nom) {
		nomEquip = nom;
		puntsLliga = 0;
		numEquips++;
	}
	
	public String getNom(){
		return nomEquip;
	}
	
	public int getPunts(){
		return puntsLliga;
	}
	

	public void incrementaPunts(int punts){
		puntsLliga += punts;
	}

	@Override
	public int compareTo(Equip o) {
		if (this.getPunts()>o.getPunts())
		return -1;
		return 1;
	}
}