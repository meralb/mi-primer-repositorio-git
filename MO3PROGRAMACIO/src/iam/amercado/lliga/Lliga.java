package iam.amercado.lliga;

import java.util.Arrays;
import java.util.Random;

public class Lliga {
	Equip[] equips;
	Partit[] partits;
	
	public Lliga(Equip[] eq) {
		int n = eq.length, numPartit = 0;
		equips = eq;
		partits = new Partit[n*n-n];
		
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				if(j!=i){
					partits[numPartit++] = new Partit(equips[i],equips[j]);
				}
			}
		}
		
	}
	
	public void jugaPartit(int numPartit){
		Random rand = new Random();
		int randomLocal = rand.nextInt(6);
		int randomVisitant = rand.nextInt(6);
		
		for(int i=0;i<randomLocal;i++){
			partits[numPartit].marcaLocal();
		}
		
		for(int i=0;i<randomVisitant;i++){
			partits[numPartit].marcaVisitant();
		}
		
		partits[numPartit].fin();
	}
	
	public void jugarLliga(){
		for(int i=0;i<partits.length;i++){
			jugaPartit(i);
		}
	}
	
	public String classificacio(){
	     String classificacio = "";
	     Arrays.sort(equips);
	     for(int i=0;i<equips.length;i++){
	    	 classificacio = classificacio + "Nom " + equips[i].getNom() + "     Punts " + equips[i].getPunts() + "\n";
	     }
	     
	     return classificacio;
	}

}