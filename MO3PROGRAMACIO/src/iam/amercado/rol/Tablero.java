package iam.amercado.rol;
import java.util.ArrayList;

public class Tablero {

	ArrayList<Jugador> jugadors;
	int ronda;
	int numCasillas = 60;
	String historial;

	public Tablero(ArrayList jugadors) {
		this.jugadors = jugadors;
	}

	public boolean nueva_ronda() {
		ronda++;
		for (Jugador j : jugadors) {

			historial += "\n" + j.desplaza(j.tirada());
			if (j.posicion() >= numCasillas) {
				return true;
			}
		}
		return false;
	}

	public String mostrarHistorial() {
		return historial;
	}
}
