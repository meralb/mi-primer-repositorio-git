package iam.amercado.rol;

public abstract class Jugador implements Dibuixable {

	String nom;
	int posicion;
	static int record;

	public Jugador(String nombre) {
		this.nom = nombre;
		posicion = 0;
	}

	public int posicion() {
		return posicion;
	}

	public String muere() {
		posicion = 0;
		return "Jugador" + nom + "vuelve a la salida";
	}

	public String desplaza(int numCasillas) {
		posicion += numCasillas;

		if (posicion > record) {
			record = posicion;
			return "Jugador " + nom + " se ha desplazado " + numCasillas + " casillas y va en la posicion " + posicion
					+ " PRIMERO";
		} else {
			return "Jugador " + nom + " se ha desplazado " + numCasillas + " casillas y va en la posicion " + posicion;
		}

	}

	public abstract int tirada();

	public String toString() {
		if (posicion == record) {
			return nom + "est� en la posicion " + posicion + "PRIMERO";
		} else {
			return nom + "est� en la posicion " + posicion;
		}
	}
	
	@Override
	public String dibuixa() {
		
		String foto="Url imatge";
			return foto+posicion+nom;
	}

}
