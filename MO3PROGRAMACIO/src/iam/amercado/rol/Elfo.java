package iam.amercado.rol;

public class Elfo extends Jugador {

	public Elfo(String nombre) {
		super(nombre);
	}

	public String toString() {
		return super.toString() + " Elfos";
	}

	@Override
	public int tirada() {
		int random = (int) ((Math.random() * 6) + 1);
		int random2 = (int) ((Math.random() * 6) + 1);
		return random + random2 + 1;
	}


}
