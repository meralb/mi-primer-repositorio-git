package iam.amercado.abstractes;

public class Rectangle extends Shape {
	protected double width;
	protected double length;

	public Rectangle() {

	}

	public Rectangle(double width, double length) {
		this.width = width;
		this.length = length;

	}

	public Rectangle(double width, double length, String color, boolean filled) {
		super(color, filled);
		this.width = width;
		this.length = length;

	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getArea() {
		double area;
		area = length * width;
		return area;
	}

	public double getPerimeter() {
		double perimeter;
		perimeter = (length * 2) + (width * 2);
		return perimeter;
	}

	public String toString() {
		return "Rectangle";
	}

}
