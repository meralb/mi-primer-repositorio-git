package iam.amercado.biblioteca;

public class Volum extends Obra {
	private short nro;
	
	public Volum(String referencia, String titol, String autor, short nrePags, short nro){
		super(referencia,titol, autor, nrePags);
		this.nro=nro;
	}
	
	public String getReferencia() {
		return super.getReferencia();
	}
	
	public void setReferencia(String referencia) {
		 super.setReferencia(referencia);
	}
	
	public String getTitol() {
		return super.getTitol();
	}
	
	public void setTitol(String titol) {
		 super.setTitol(titol);
	}

	public short getNro() {
		return nro;
	}

	public void setNro(short nro) {
		this.nro = nro;
	}

	@Override
	public String toString() {
		return "Volum [nro=" + nro + "]"+super.toString();
	}


	
}
