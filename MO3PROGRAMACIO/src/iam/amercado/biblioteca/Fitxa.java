package iam.amercado.biblioteca;
public class Fitxa {
	protected String referencia;
	protected String titol;
	static int contadorFitxes=0;
	
	//public Fitxa(){}
	
	public Fitxa(String referencia,String titol ){
		this.referencia=referencia;
		this.titol=titol;
		contadorFitxes++;
	}
	
	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getTitol() {
		return titol;
	}

	public void setTitol(String titol) {
		this.titol = titol;
	}

	@Override
	public String toString() {
		return "Fitxa [referencia=" + referencia + ", titol=" + titol + "]";
	}

	public boolean equals(Object b){
		if (b instanceof Fitxa) {
			Fitxa c = (Fitxa)b;
				if (referencia==c.getReferencia())
					return true;
		}
		return false;		
	}
	public static int contadorFitxes(){
		return contadorFitxes;
	}
}

