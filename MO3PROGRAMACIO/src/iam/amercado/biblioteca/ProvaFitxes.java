package iam.amercado.biblioteca;
public class ProvaFitxes {

	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Volum volum = new Volum("carles","La biografia","Carlitos Brawn", (short)1996,(short)1);
		Volum volum2 = new Volum("carles","La biografia","Carlitos Brawn", (short)1996,(short)1);

		
		
		Fitxa fitxa1 = new Fitxa("carles","La biografia");
		Fitxa fitxa2 = new Fitxa("carles","La biografia");
		
		
		
		Obra obra1 = new Obra("carles","La biografia","Carlitos Brawn", (short)1996);
		Obra obra2 = new Obra("carlestiti","La biografia","Carlitos Brawn", (short)1996);

		
		
		Revista revista = new Revista("carles","La biografia", (short)1996,(short)1);
		Revista revista2 = new Revista("carles","La biografia", (short)1996,(short)1);
		
		System.out.println("El numero de fitxes al sistema és: "+Fitxa.contadorFitxes());
		System.out.println(volum.equals(volum2));
		System.out.println(fitxa1.equals(fitxa2));
		System.out.println(obra1.equals(obra2));
		System.out.println(revista.equals(revista2));	
	}

}
