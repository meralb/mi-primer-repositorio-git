package iam.amercado.biblioteca;
public class Obra extends Fitxa {
	protected String autor;
	protected short nrePags;
	
	public Obra (String referencia,String titol, String autor,short nrePags){
		super(referencia,titol);
		this.autor=autor;
		this.nrePags=nrePags;
		
	}
	
	public String getReferencia() {
		return super.getReferencia();
	}
	
	public void setReferencia(String referencia) {
		 super.setReferencia(referencia);
	}
	
	public String getTitol() {
		return super.getTitol();
	}
	public void setTitol(String titol) {
		 super.setTitol(titol);
	}
	
	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public short getNrePags() {
		return nrePags;
	}

	public void setNrePags(short nrePags) {
		this.nrePags = nrePags;
	}


	@Override
	public String toString() {
		return "Obra [autor=" + autor + ", nrePags=" + nrePags + "]";
	}
}
