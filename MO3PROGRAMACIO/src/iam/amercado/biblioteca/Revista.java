package iam.amercado.biblioteca;
public class Revista extends Fitxa{
	private short any;
	private short nro;
	
	public Revista(String referencia,String titol,short any, short nro){
		super(referencia,titol);
		this.any=any;
		this.nro=nro;
	}
	public String getReferencia() {
		return super.getReferencia();
	}
	
	public void setReferencia(String referencia) {
		 super.setReferencia(referencia);
	}
	
	public String getTitol() {
		return super.getTitol();
	}
	public void setTitol(String titol) {
		 super.setTitol(titol);
	}
	
	public short getAny() {
		return any;
	}
	
	public void setAny(short any) {
		this.any = any;
	}

	public short getNro() {
		return nro;
	}

	public void setNro(short nro) {
		this.nro = nro;
	}
	@Override
	public String toString() {
		return "Revista [any=" + any + ", nro=" + nro + "]"+super.toString();
	}


}
