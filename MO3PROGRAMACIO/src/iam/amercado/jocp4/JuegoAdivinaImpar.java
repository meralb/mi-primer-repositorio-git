package iam.amercado.jocp4;

public class JuegoAdivinaImpar extends JuegoAdivinaNumero implements Jugable {

	public JuegoAdivinaImpar(int vidas, int numAdivi) {
		super(vidas, numAdivi);
	}

	public void MuestraNombre() {
		super.MuestraNombre();
		System.out.println("Adivina un numero impar");

	}

	public void MuestraInfo() {
		super.MuestraInfo();
		System.out.println("Intenta adivinar un numero entre 0 i 10");
		System.out.println("Et queden "+ numVidasIniciales+"intents");
	}
}
