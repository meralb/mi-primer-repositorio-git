package iam.amercado.jocp3;

public class Aplicacion {

	public static void main(String[] args) {

		JuegoAdivinaNumero jan = new JuegoAdivinaNumero(3, 6);
		JuegoAdivinaPar jap = new JuegoAdivinaPar(3, 2);
		JuegoAdivinaImpar jai = new JuegoAdivinaImpar(3, 5);

		jan.ValidaNumero(6);
		jap.ValidaNumero(2);
		jai.ValidaNumero(5);
		jan.Juega();
		jap.Juega();
		jai.Juega();
		jan.MuestraVidasRestantes();
	}

}
