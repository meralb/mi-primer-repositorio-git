package iam.amercado.jocp3;

import java.util.Scanner;

public class JuegoAdivinaNumero extends Juego {

	int numAdivinar;

	public JuegoAdivinaNumero(int vidas, int numAdivi) {
		super(vidas);
		this.numAdivinar = numAdivi;
	}

	boolean ValidaNumero(int num) {
		return true;
	}

	public void Juega() {
		ReiniciaPartida();
		boolean aux = false;

		do {
			System.out.println("Adivina un numero entre 0 i 10");
			Scanner numllegit = new Scanner(System.in);

			int num = Integer.parseInt(numllegit.nextLine());

			if (num == numAdivinar) {
				System.out.println("Acertaste!");
				ActualizaRecord();
				aux = true;
				break;
			}
			if (ValidaNumero(num) == false) {
				System.out.println("Introdueix un altre numero");
				Scanner numllegit2 = new Scanner(System.in);

				int num2 = Integer.parseInt(numllegit.nextLine());
				break;
			}

			else if (num != numAdivinar) {
				aux = QuitaVida();
				if (aux) {
					if (num > numAdivinar) {
						System.out.println("El numero es mes petit, segeix intentant");
					} else {
						System.out.println("El numero es mes gran, segueix intentant");
					}
				}
			} else {

			}
		} while (aux != false);

	}

}