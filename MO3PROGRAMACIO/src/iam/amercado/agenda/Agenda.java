package iam.amercado.agenda;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Agenda {

	private HashMap<String, Persona> mapPersona;

	public Agenda() {
		mapPersona = new HashMap<>();
	}

	public void addPersona(Persona p) {
		if (mapPersona.containsKey(p.getNif())) {
			System.out.println("Aquesta persona ja la tens a l'agenda");
			mapPersona.get(p.getNif()).setTel(p.getTel());

		} else {
			mapPersona.put(p.getNif(), p);
		}

	}

	public void removePersona(Persona p) {
		mapPersona.remove(p.getNif());
	}

	public Persona buscarNif(String k) {
		return mapPersona.get(k);
	}

	public void buscarNom(String nom) {
		Set set = mapPersona.entrySet();
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry mapEntry = (Map.Entry) iterator.next();
			if (nom.equals(mapPersona.get(mapEntry.getKey()).getNom())) {
				System.out.print(mapPersona.get(mapEntry.getKey()).toString());

			}

		}
	}

	public void llistar() {
		Set set = mapPersona.entrySet();
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry mapEntry = (Map.Entry) iterator.next();
			System.out.print(mapPersona.get(mapEntry.getKey()).toString());

		}

	}
}
