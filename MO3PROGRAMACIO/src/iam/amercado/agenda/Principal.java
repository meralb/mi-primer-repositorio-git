package iam.amercado.agenda;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {

		Scanner scann = new Scanner(System.in);

		Agenda agenda = new Agenda();
		Persona persona = new Persona();
		int opcio ;
		do {
			System.out.println("Menu:");
			System.out.println("1) Afegir una persona.");
			System.out.println("2) Eliminar una persona.");
			System.out.println("3) Buscar persona per Nif.");
			System.out.println("4) Buscar persona per Nom.");
			System.out.println("5) Llistar persones.");
			System.out.println("6) Sortir.");
			
			System.out.println("\nEscull una opcio.");

			
			opcio = scann.nextInt();

			switch (opcio) {

			case 1:
				agenda.addPersona(persona.crear());
				break;
			case 2:
				agenda.removePersona(persona.crear());
				break;
			case 3:
				System.out
						.println("Introdueix el Nif de la persona que vols buscar: ");
				String nif = scann.nextLine();
				agenda.buscarNif(nif).toString();
				break;
			case 4:
				System.out
						.println("Introdueix el Nom de la persona que vols buscar: ");
				String nom = scann.nextLine();
				agenda.buscarNom(nom);
				break;
			case 5:
				agenda.llistar();
				break;

			}
		} while (opcio != 6);
	}
}
