package iam.amercado.agenda;

import java.util.Scanner;

public class Persona {
	private String nom,tel,nif;
	
	public Persona(String nom,String tel,String nif){
		this.nom=nom;
		this.tel=tel;
		this.nif=nif;
	}

	public Persona(){}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	@Override
	public String toString() {
		return "Persona [nom=" + nom + ", tel=" + tel + ", nif=" + nif + "]\n";
	}
	
	public Persona crear(){
		
		Scanner sc= new Scanner(System.in); 
		System.out.println("Introdueix les dades de la persona: \n");
		System.out.println("Nom: ");
		String nom=sc.nextLine();
		System.out.println("Telefon: ");
		String tel=sc.nextLine();
		System.out.println("Nif: ");
		String nif=sc.nextLine();
		Persona persona=new Persona(nom, tel, nif);
		return persona;
	}

}
