package iam.amercado.PFinal2;

import java.util.ArrayList;
import java.util.Collections;

public class Magatzem {

	private ArrayList<Article> magatzem;

	public Magatzem(ArrayList<Article> productes) {
		magatzem = productes;
	}

	public Article buscarProducte(int codi) {

		for (int i = 0; i < magatzem.size(); i++) {
			if (magatzem.get(i).getCodi() == codi) {
				return magatzem.get(i);
			}
		}

		return null;
	}

	public ArrayList<Article> noStock() {

		ArrayList<Article> esgotats = new ArrayList<>();

		for (int i = 0; i < magatzem.size(); i++) {
			if (magatzem.get(i).getStock() < 1 || magatzem.get(i).isDemanarStock()) {
				esgotats.add(magatzem.get(i));
			}
		}

		return esgotats;
	}

	public String mostrarProductes() {

		String productes = null;

		for (int i = 0; i < magatzem.size(); i++) {
			if (i == 0) {
				productes = magatzem.get(i).toString();
			} else {
				productes += "\n" + magatzem.get(i).toString();
			}

		}

		return productes;
	}

	public String sort() {

		ArrayList<Article> ordenats = new ArrayList<>();

		ordenats = magatzem;

		Collections.sort(ordenats);

		String productes = null;

		for (int i = 0; i < ordenats.size(); i++) {
			if (i == 0) {
				productes = ordenats.get(i).toString();
			} else {
				productes += "\n" + ordenats.get(i).toString();
			}

		}

		return productes;
	}

}
