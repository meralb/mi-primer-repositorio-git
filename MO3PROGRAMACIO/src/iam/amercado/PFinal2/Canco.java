package iam.amercado.PFinal2;

public class Canco {

	private String titol;
	private int duracio;

	public Canco(String titol, int duracio) {
		this.titol = titol;
		this.duracio = duracio;
	}

	public String getTitol() {
		return titol;
	}

	public int getDurada() {
		return duracio;
	}

	@Override
	public String toString() {
		return "Titol: " + titol + duracio + " segs";
	}

}
