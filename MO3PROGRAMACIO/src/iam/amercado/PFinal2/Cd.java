package iam.amercado.PFinal2;

import java.util.ArrayList;

public class Cd extends Article {

	private String titol, nomBanda;
	private ArrayList<Canco> cancons;

	public Cd(int codi, String descripcio, double preu, int stock, String titol, String nomBanda,
			ArrayList<Canco> cancons) {
		super(codi, descripcio, preu, stock);
		this.titol = titol;
		this.nomBanda = nomBanda;
		this.cancons=cancons;
		setNumCan(cancons.size());
	}

	public String getTitol() {
		return titol;
	}

	public String getNomBanda() {
		return nomBanda;
	}

	@Override
	public String mostraContingut() {
		String infoArticle;
		infoArticle = "Cd: "+getTitol()+" - Grup: " + getNomBanda() + " - Cançons: ";
		for (int i = 0; i < cancons.size(); i++) {
			if (i == 0) {
				infoArticle += cancons.get(i).toString();
			} else {
				infoArticle += ", " + cancons.get(i).toString();
			}
		}
		return infoArticle;
	}
	
	@Override
	public String toString() {
		return mostraContingut() + " ,codi: " + getCodi() + " ,descripcio: " + getDescripcio() + " ,preu: "
				+ getPreu() + " €,  Stock: " + getStock();
	}

}
