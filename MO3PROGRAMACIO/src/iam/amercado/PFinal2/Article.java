package iam.amercado.PFinal2;

public abstract class Article implements Comparable<Article> {

	private int codi;
	private String descr;
	private double preu;
	private int stock;
	private boolean demanarStock = false;

	private static int numCan = 0;

	public Article(int codi, String descr, double preu, int stock) {
		this.codi = codi;
		this.descr = descr;
		this.preu = preu;
		this.stock = stock;
	}

	public int getCodi() {
		return codi;
	}

	public String getDescripcio() {
		return descr;
	}

	public double getPreu() {
		return preu;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock += stock;
	}

	public double venta(int numArticles) {
		double total = 0;

		if (getStock() >= numArticles) {
			total = preu * numArticles;
			setStock(numArticles * (-1));
			return total;
		}
		setDemanarStock(true);
		return -1;
	}

	public void repostar(int quantitat) {
		stock += quantitat;
		setDemanarStock(false);
	}

	public int getNumCan() {
		return numCan;
	}

	public void setNumCan(int can) {
		numCan += can;
	}

	public int compareTo(Article o) {
		if (o.getCodi() > this.getCodi()) {
			return -1;
		}
		return 1;
	}

	public boolean isDemanarStock() {
		return demanarStock;
	}

	public void setDemanarStock(boolean demanarStock) {
		this.demanarStock = demanarStock;
	}

	public abstract String mostraContingut();

}
