package iam.amercado.PFinal2;

public class Dvd extends Article {

	private String titol, director, vO;
	private int duracio;

	public Dvd(int codi, String descripcio, double preu, int stock, String titol, String director,
			String vO, int duracio) {
		super(codi, descripcio, preu, stock);
		this.titol = titol;
		this.director = director;
		this.vO = vO;
		this.duracio = duracio;
	}

	public String getTitol() {
		return titol;
	}

	public String getDirector() {
		return director;
	}

	public String getIdiomaOriginal() {
		return vO;
	}

	public int getDurada() {
		return duracio;
	}

	@Override
	public String mostraContingut() {
		String contingut;
		contingut= "Dvd Titol: "+getTitol()+" - Director: "+getDirector()+" ("+getDurada()+" minuts)";
		return contingut;
	}

	@Override
	public String toString() {
		return "Dvd Titol: " + titol + " - Director: " + director + " - Idioma Original: " + vO + " - Durada: "
				+ duracio + "mins - Codi: " + getCodi() + " - Descripcio: " + getDescripcio() + " - Preu: "
				+ getPreu() + " euros - Stock: " + getStock();
	}
	


}
