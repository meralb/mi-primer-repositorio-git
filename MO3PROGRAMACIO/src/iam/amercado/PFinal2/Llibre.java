package iam.amercado.PFinal2;

public class Llibre extends Article{

	private String titol, autor;
	private int numPags;
	
	public Llibre(int codi, String descripcio, double preu, int stock, String titol, String autor, int numPags) {
		super(codi, descripcio, preu, stock);
		this.titol = titol;
		this.autor = autor;
		this.numPags = numPags;
	}

	public String getTitol() {
		return titol;
	}

	public String getAutor() {
		return autor;
	}

	public int getNumPags() {
		return numPags;
	}

	@Override
	public String mostraContingut() {
		String contingut;
		contingut = getTitol()+" - "+getAutor()+" ("+getNumPags()+" pags)";
		return contingut;
	}
	
	public String toString() {
		return "Llibre Titol: " + titol + " - Autor: " + autor + " - Pags: "
				+ numPags + " - Codi: " + getCodi() + " - Descripcio: " + getDescripcio() + " - Preu: "
				+ getPreu() + " euros - Stock: " + getStock();
	}
	
}
