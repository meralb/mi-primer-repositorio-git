package iam.amercado.herencia;

public class TestObjecteGeometric {

	public static void main(String[] args) {
		Cercle cercle;
		Quadrat quadrat;
		
		cercle = new Cercle();
		System.out.println(cercle + "\nPerimetre = " + cercle.getPerimetre() + " Area = "+cercle.getArea());
		
		quadrat = new Quadrat();
		System.out.println(quadrat + "\nPerimetre = " + quadrat.getPerimetre() + " Area = "+quadrat.getArea());

		cercle = new Cercle(10);
		System.out.println(cercle + "\nPerimetre = " + cercle.getPerimetre() + " Area = "+cercle.getArea());
		
		quadrat = new Quadrat(15);
		System.out.println(quadrat + "\nPerimetre = " + quadrat.getPerimetre() + " Area = "+quadrat.getArea());
		
		cercle = new Cercle(5, 3, 3, "verd");
		System.out.println(cercle + "\nPerimetre = " + cercle.getPerimetre() + " Area = "+cercle.getArea());
		
		quadrat = new Quadrat(15, 4, 4, "vermell");
		System.out.println(quadrat + "\nPerimetre = " + quadrat.getPerimetre() + " Area = "+quadrat.getArea());

	}

}
