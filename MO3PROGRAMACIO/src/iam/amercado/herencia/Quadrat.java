package iam.amercado.herencia;

public class Quadrat extends ObjecteGeometric {
	private int costat;
	
	public Quadrat(int costat) {
		super();
		this.costat = costat;
	}
	
	public Quadrat(){
		super();
		costat = 1;
	}
	
	public Quadrat(int costat, int x, int y, String color){
		super(x,y,color);
		this.costat = costat;
	}
	
	public int getPerimetre(){
		return 4*costat;
	}
	
	public int getArea(){
		return costat*costat;
	}

	@Override
	public String toString() {
		return "Quadrat de costat=" + costat + " " + super.toString();
	}

}
