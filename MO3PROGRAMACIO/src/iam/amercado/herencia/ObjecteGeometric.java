package iam.amercado.herencia;

public abstract class ObjecteGeometric {
	private int x, y;
	private String color;
	

	
	public ObjecteGeometric(int x, int y, String color){
		this.x = x;
		this.y = y;
		this.color = color;
		
	}
	
	public ObjecteGeometric() {
		x = 1;
		y = 1;
		color = "black";
	}

	@Override
	public String toString() {
		return "Objecte Geometric amb posicio x=" + x + ", posicio y=" + y + ",i color=" + color;
	}

}
