package iam.amercado.herencia;
import java.lang.Math;

public class Cercle extends ObjecteGeometric {
	private int radi;
	
	public Cercle(int radi) {
		super();
		this.radi = radi;
	}
	
	public Cercle(){
		super();
		radi = 1;
	}
	
	public Cercle(int radi, int x, int y, String color){
		super(x,y,color);
		this.radi = radi;
	}
	
	public double getPerimetre(){
		return 2*Math.PI*radi;
	}
	
	public double getArea(){
		return Math.PI*radi*radi;
	}

	@Override
	public String toString() {
		return "Cercle de radi=" + radi + " " + super.toString();
	}

}
