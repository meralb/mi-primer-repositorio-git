package iam.amercado.partit;

public class Equip {
	private String nomEquip;
	private int punts;

	public Equip(String nomEquip) {
		
		this.nomEquip = nomEquip;
		this.punts =0;
		Lliga.numequips++;
	}


	public String getNom() {
		return nomEquip;
	}


	public int getPunts() {
		return punts;
	}
	
	public void incrementaPunts (int punts){
		this.punts=this.punts+punts;
	}

}
