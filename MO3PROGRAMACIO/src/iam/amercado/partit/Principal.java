package iam.amercado.partit;

public class Principal {

	public static void main(String[] args) {
		
		Equip eMadrid = new Equip("madrid");
		Equip eBarcelona = new Equip("barcelona");

		Partit p = new Partit(eBarcelona,eMadrid);
		
		p.marcaLocal();
		p.marcaVisitant();
		p.marcaVisitant();
		p.marcaLocal();
		p.marcaLocal();
		p.marcaLocal();
		p.marcaLocal();
		p.marcaLocal();
		System.out.println(p.fin());
		System.out.println(p.marcador());

		
	}

}
