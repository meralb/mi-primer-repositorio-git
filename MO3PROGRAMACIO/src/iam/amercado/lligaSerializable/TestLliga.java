package iam.amercado.lligaSerializable;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import iam.amercado.escola.Grup;

public class TestLliga {

	public static void main(String[] args) {

		Equip[] equips;
		Scanner sca = new Scanner(System.in);
		int numEquips;

		System.out.println("Quantitat d'equips: ");
		numEquips = sca.nextInt();
		equips = new Equip[numEquips];

		for (int i = 0; i < numEquips; i++) {
			String nom;
			System.out.println("Introdueix el nom de l'equip " + i);
			nom = sca.next();
			equips[i] = new Equip(nom);
		}

		Lliga lliga = new Lliga(equips);
		try {

			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("lliga1"));
			System.out.println("1");
			out.writeObject(lliga);
			System.out.println("2");
			out.close();
			System.out.println("3");

			for (int j = 0; j < equips.length; j++) {
				System.out.println(j);

				ObjectOutputStream outEquips = new ObjectOutputStream(new FileOutputStream("Equip" + (j + 1)));
				System.out.println("4");
				outEquips.writeObject(equips[j]);
				System.out.println("5");
				outEquips.close();
				System.out.println("6");
			}

			Lliga lliga2;
			ObjectInputStream in;
			in = new ObjectInputStream(new FileInputStream("lliga1"));
			lliga2 = (Lliga) in.readObject();
			in.close();

			System.out.println(lliga2.toString());
			lliga.jugarLliga();

			System.out.println(lliga.classificacio());
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

}
