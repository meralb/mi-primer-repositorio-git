package iam.amercado.Exercici2i3;

import java.util.ArrayList;
import java.util.Scanner;

import iam.amercado.Exercici2i3.Elfo;
import iam.amercado.Exercici2i3.Enano;
import iam.amercado.Exercici2i3.Jugador;

public class Principal {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int numJugadors;
		String tipusJugador;
		String nomEnano;
		String nomElfo;
		ArrayList<Jugador> jugadors = new ArrayList<Jugador>();
		Enano enano;
		Elfo elfo;

		System.out.println("Digues el numero de jugadors que vols: ");
		numJugadors = Integer.parseInt(input.nextLine());

		for (int i = 0; i < numJugadors; i++) {

			System.out.println("Jugador " + (i + 1) + ": Escull si vols Elfo o Enano: ");
			tipusJugador = input.nextLine();
			if (tipusJugador.equals("enano")) {
				System.out.println("Introdueix el nom del enano: ");
				nomEnano = input.nextLine();
				enano = new Enano(nomEnano);
				jugadors.add(enano);
			} else if (tipusJugador.equals("elfo")) {
				System.out.println("Introdueix el nom del elfo: ");
				nomElfo = input.nextLine();
				elfo = new Elfo(nomElfo);
				jugadors.add(elfo);
			} else {
				System.out.println("No existeix aquest tipus de jugador");
			}
		}

		Tablero tablero = new Tablero(jugadors);

		boolean winner;
		do {
			winner = tablero.nuevaRonda();
		} while (!winner);

		System.out.println(tablero.mostrarHistorial());

	}

}
