package iam.amercado.Exercici2i3;

import java.util.Random;

public abstract class Jugador implements Dibuixable {

	private String nomJugador;
	private int posicioTablero = 0;
	private int record;

	public Jugador(String nom) {
		this.nomJugador = nom;
	}

	public int posicion() {
		return posicioTablero;
	}

	public String muere() {
		posicioTablero = 0;
		return "Jugador " + nomJugador + " vuelve a la salida";
	}

	public String desplaza(int numCasillas) {
		posicioTablero += numCasillas;
		if (posicioTablero > record) {
			return "Jugador " + nomJugador + " se ha desplazado " + numCasillas + " casillas y va en la posición "
					+ posicioTablero + " i va Primero!!\n";
		}
		return "Jugador " + nomJugador + " se ha desplazado " + numCasillas + " casillas y va en la posición "
				+ posicioTablero;
	}

	public abstract int tirada();

	public String toString() {
		if (posicioTablero == record) {
			return nomJugador + " esta en la posicio " + posicioTablero + " i va PRIMER!!";
		}
		return nomJugador + " esta en la posicio " + posicioTablero;
	}

	public String dibuixaJugador() {
		return "Ruta de la imatge " + posicioTablero + " " + nomJugador;

	}
}
