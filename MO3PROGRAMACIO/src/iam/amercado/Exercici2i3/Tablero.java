package iam.amercado.Exercici2i3;

import java.util.ArrayList;

public class Tablero {

	private ArrayList<Jugador> jugadors;
	private int rondaActual;
	private int numCasillas=200;
	private String historial;

	public Tablero(ArrayList<Jugador> jugadors) {
		this.jugadors = jugadors;
	}

	public boolean nuevaRonda() {
		for (Jugador jugadors : jugadors) {

			historial += jugadors.desplaza(jugadors.tirada());
			if (jugadors.posicion() == numCasillas) {
				return true;
			}
		}
		return false;
	}

	public String mostrarHistorial() {
		return historial;
	}
}
