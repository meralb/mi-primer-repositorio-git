package iam.amercado.Exercici2i3;

public class Elfo extends Jugador {

	public Elfo(String nombre) {
		super(nombre);
	}

	@Override
	public int tirada() {
		int rand1 = (int)((Math.random() * 6) + 1);
		int rand2 = (int)((Math.random() * 6) + 1);
		return rand1+rand2+1;
	}
	
	@Override
	public String toString() {
		return super.toString()+ " elfo";
	}
}
