package iam.amercado.Exercici1;

public class Cancons{
	String titol;
	int duracio;
	public Cancons(String titol, int duracio) {
		this.titol = titol;
		this.duracio = duracio;
	}
	public String toString(){
		return "\tCan�o: " + titol + "\n\tDuracio: " + duracio + " segons\n"; 
	}
}
