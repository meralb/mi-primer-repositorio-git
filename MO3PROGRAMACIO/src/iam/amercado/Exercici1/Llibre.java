package iam.amercado.Exercici1;

public class Llibre extends Article{
	String titol,autor;
	int pgs;
	public Llibre(int codi, int stock, double preu, String descripcio, String titol, String autor, int pagines) {
		super(codi, stock, preu, descripcio);
		this.titol = titol;
		this.autor = autor;
		this.pgs = pagines;
	}
	public String toString(){
		return "Llibre - " + titol + "\nAutor: " + autor + "\nPagines: " + pgs + " pagines\n";
	}
}
