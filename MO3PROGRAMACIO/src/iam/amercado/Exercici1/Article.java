package iam.amercado.Exercici1;

abstract class Article implements Comparable<Article> {
	int codi, stock;
	double preu;
	String desc;
	private boolean demanarStock = false;


	public boolean isDemanarStock() {
		return demanarStock;
	}

	public void setDemanarStock(boolean demanarStock) {
		this.demanarStock = demanarStock;
	}

	public Article(int codi, int stock, double preu, String descr) {
		this.codi = codi;
		this.stock = stock;
		this.preu = preu;
		this.desc = descr;
	}

	public int getCodi() {
		return codi;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public double getPreu() {
		return preu;
	}

	public void setPreu(double preu) {
		this.preu = preu;
	}

	public String getDescripcio() {
		return desc;
	}

	public void setCodi(int codi) {
		this.codi = codi;
	}


	public double venta(int numArticles) {
		double total = 0;

		if (getStock() >= numArticles) {
			total = preu * numArticles;
			setStock(numArticles * (-1));
			return total;
		}
		setDemanarStock(true);
		return -1;
	}

	public void repostar(int quantitat) {
		stock += quantitat;
		setDemanarStock(false);
	}

	@Override
	public int compareTo(Article o) {
		if (this.codi < o.codi)
			return -1;
		if (this.codi > o.codi)
			return 1;
		return 0;
	}

}
