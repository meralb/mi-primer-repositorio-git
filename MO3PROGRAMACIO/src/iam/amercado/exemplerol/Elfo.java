package iam.amercado.exemplerol;

public class Elfo extends Jugador {

	public Elfo(String nombre) {
		super(nombre);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int tirada() {
		int random = (int)((Math.random() * 6) + 1);
		int random2 = (int)((Math.random() * 6) + 1);
		return random+random2+1;
	}
	
	@Override
	public String toString() {
		return super.toString()+ ". Elfo";
	}
}
