package iam.amercado.exemplerol;

public abstract class Jugador {

	private String nomJugador;
	private int posicion;
	private int record;

	public Jugador(String nom) {
		this.nomJugador = nom;
		posicion = 0;
	}

	public int posicion() {
		return posicion;
	}

	public String muere() {
		posicion = 0;
		return "Jugador " + nomJugador + " vuelve a la salida";
	}

	public String desplaza(int numero_casillas) {
		posicion += numero_casillas;
		if (posicion > record) {
			record = posicion;
			return "Jugador " + nomJugador + " se ha desplazado " + numero_casillas + " casillas y va en la posici�n "
					+ posicion + ". PRIMERO";
		}
		return "Jugador " + nomJugador + " se ha desplazado " + numero_casillas + " casillas y va en la posici�n "
				+ posicion + ".";
	}

	public abstract int tirada();

	public String toString() {
		if (posicion == record) {
			return nomJugador + " est� en la posici�n " + posicion + ". PRIMERO";
		}
		return nomJugador + " est� en la posici�n " + posicion;
	}
}
