package iam.amercado.exemplerol;

import java.util.ArrayList;

public class Tablero {
	
	private ArrayList<Jugador> jugadores;
	private int ronda_actual;
	private int num_cas_tablero;
	private String historial="";
	
	public Tablero(ArrayList<Jugador> jugadores) {
		this.jugadores=jugadores;
	}
	
	public boolean nueva_ronda(){
		for(Jugador j: jugadores){
			
			historial+="\n" + j.desplaza(j.tirada());
			if(j.posicion()==num_cas_tablero){
				return true;
			}
		}
		return false;
	}
	
	public String mostrarHistorial(){
		return historial;
	}
}
