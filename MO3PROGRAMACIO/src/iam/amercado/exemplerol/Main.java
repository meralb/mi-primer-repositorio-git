package iam.amercado.exemplerol;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int num_jug;
		String tipo;
		ArrayList<Jugador> j = new ArrayList<Jugador>();
		Enano e;
		Elfo elf;
		
		
		System.out.println("Cuantos jugadores quieres?");
		num_jug=Integer.parseInt(input.nextLine());
		
		for (int i = 0; i < num_jug; i++) {
			
			System.out.println("Jugador -"+ i+1+ ": Elige su tipo: ");
			tipo = input.nextLine();
			if(tipo.equalsIgnoreCase("enano")){
				 e = new Enano("pepe");
				 j.add(e);
			}else if(tipo.equalsIgnoreCase("elfo")){
				elf = new Elfo("jeje");
				j.add(elf);
			}else{
				System.out.println("Introduce un tipo correcto");
			}
		}
		
		Tablero t = new Tablero(j);
		
		while(t.nueva_ronda()){
			t.nueva_ronda();
		}
		System.out.println(t.mostrarHistorial());

	}

}
