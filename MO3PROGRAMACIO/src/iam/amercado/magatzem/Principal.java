package iam.amercado.magatzem;

import java.util.ArrayList;

public class Principal {
	public static void main(String[] args) {
		ArrayList<Cancons> songs = new ArrayList<Cancons>(); 
		
		Dvd dvd1 = new Dvd(1,5,16,"Pelicula animals","El rei lleo","Disney","Espa�ol",90);
		Dvd dvd2 = new Dvd(2,9,17,"Pelicula animals","El rei lleo 2","Disney","Espa�ol",90);
		
		Llibre llibre1 = new Llibre(3,1,20,"Ciencia ficcio","Star-Wars EP I","George Lucas",336);
		Llibre llibre2 = new Llibre(4,0,20,"Ciencia ficcio","Star-Wars EP II","John R.R. Tolkien",384);
		
		Cancons song1 = new Cancons("Song for Someone",208);
		Cancons song2 = new Cancons("Volcano",188);
		songs.add(song1);
		songs.add(song2); 
		
		Cds cd1 = new Cds(5,20,22,"Big Hits","Songs of Innocence","U2",11,songs);
		
		System.out.println(dvd1.toString());
		System.out.println(dvd2.toString());
		System.out.println(llibre1.toString());
		System.out.println(llibre2.toString());
		System.out.println(cd1.toString());
		
		ArrayList<Article> articles = new ArrayList<Article>();
		articles.add(dvd1);
		articles.add(dvd2);
		articles.add(llibre1);
		articles.add(llibre2);
		articles.add(cd1);
		
		Magatzem magatzem = new Magatzem(articles);
		
		System.out.println("Producte buscat: \n");
		Article search = magatzem.searchByCodi(2);
		System.out.println(search.toString());
		
		ArrayList<Article> articles1 = magatzem.noStock();
		System.out.println("Sense Stock\n");
		for(int i=0; i<articles1.size(); i++)
			System.out.println(articles1.get(i).toString());
		
		System.out.println("Llistar tots els productes:");
		System.out.println(magatzem.stringArticles());
		
		System.out.println("\nSort by codi:");
		ArrayList<Article> articles2 = magatzem.sortByCodi();
		for(int i=0; i<articles2.size(); i++)
			System.out.println("\n"+articles2.get(i).toString());
	}
}
