package iam.amercado.magatzem;

import java.util.ArrayList;
import java.util.Collections;

public class Magatzem {

	ArrayList<Article> articles;

	public Magatzem(ArrayList<Article> articles) {
		this.articles = articles;
	}

	public Article searchByCodi(int codi) {
		for (int i = 0; i < articles.size(); i++) {
			Article articles2 = articles.get(i);
			if (articles2.getCodi() == codi) {
				return articles2;
			}
		}
		return null;
	}

	public ArrayList<Article> noStock() {
		ArrayList<Article> noStock = new ArrayList<Article>();
		for (int i = 0; i < articles.size(); i++) {
			Article articles3 = articles.get(i);
			if (articles3.getStock() == 0)
				noStock.add(articles3);
		}
		return noStock;
	}

	public String stringArticles() {
		String Articles = "";
		for (int i = 0; i < articles.size(); i++) {
			Article articles4 = articles.get(i);
			String aux ="\n"+ articles4.toString();
			Articles = Articles +aux;
		}
		return Articles;
	}

	public ArrayList<Article> sortByCodi() {
		Collections.sort(articles);
		return articles;
	}

}
