package iam.amercado.magatzem;

public class Dvd extends Article{
	String titol, director,idioma;
	int duracio;
	public Dvd(int codi, int stock, double preu, String descripcio, String titol, String director, String idioma,int duracio) {
		super(codi, stock, preu, descripcio);
		this.titol = titol;
		this.director = director;
		this.idioma = idioma;
		this.duracio = duracio;
	}
	@Override
	public String toString() {
		return "DVD - " + titol + "\nDirector: " + director + "\nIdioma: " + idioma + "\nDuracio: " + duracio + " segons\n";
	}
	
}
