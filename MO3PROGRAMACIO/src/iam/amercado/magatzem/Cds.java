package iam.amercado.magatzem;

import java.util.ArrayList;

public class Cds extends Article{
	String titol, banda;
	int numSongs;
	ArrayList <Cancons> songs;
	public Cds(int codi, int stock, double preu, String descripcio, String titol, String banda, int numSongs, ArrayList<Cancons> songs) {
		super(codi, stock, preu, descripcio);
		this.titol = titol;
		this.banda = banda;
		this.numSongs = numSongs;
		this.songs = songs;
	}

	public String toString() {
		return "Cd - " + titol + "\nNom Banda: " + banda + "\nNumero de cancons: " + numSongs + "\n" + songs.toString();
	}
}
